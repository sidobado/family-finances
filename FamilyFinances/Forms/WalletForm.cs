﻿using FamilyFinances.Entity;

namespace FamilyFinances.Forms
{
    public partial class WalletForm : Form, IEntityForm<Wallet>
    {
        public WalletForm()
        {
            InitializeComponent();
            cancelButton.Click += CancelButton_Click;
            saveButton.Click += SaveButton_Click;
        }

        private void SaveButton_Click(object? sender, EventArgs e)
        {
            if (Value != null && Value.Name == nameEdit.Text)
            {
                DialogResult = DialogResult.Cancel;
                return;
            }

            if (Value == null)
            {
                _wallet = new Wallet();
            }

            Value.Name = nameEdit.Text;
            DialogResult = DialogResult.OK;
        }

        private void CancelButton_Click(object? sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private Wallet? _wallet;
        public Wallet? Value
        {
            get => _wallet;
            set
            {
                _wallet = value;
                nameEdit.Text = _wallet?.Name;
                Text = "Edycja portfela: " + _wallet?.Name;
            }
        }
    }
}
