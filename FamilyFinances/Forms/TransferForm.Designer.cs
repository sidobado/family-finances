﻿namespace FamilyFinances.Forms
{
    partial class TransferForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.sourceWalletCombo = new System.Windows.Forms.ComboBox();
            this.targetWalletCombo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.entryDateEdit = new System.Windows.Forms.DateTimePicker();
            this.amountEdit = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.amountEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 15);
            this.label2.TabIndex = 13;
            this.label2.Text = "Portfel źródłowy";
            // 
            // sourceWalletCombo
            // 
            this.sourceWalletCombo.FormattingEnabled = true;
            this.sourceWalletCombo.Location = new System.Drawing.Point(109, 12);
            this.sourceWalletCombo.Name = "sourceWalletCombo";
            this.sourceWalletCombo.Size = new System.Drawing.Size(238, 23);
            this.sourceWalletCombo.TabIndex = 1;
            // 
            // targetWalletCombo
            // 
            this.targetWalletCombo.FormattingEnabled = true;
            this.targetWalletCombo.Location = new System.Drawing.Point(109, 41);
            this.targetWalletCombo.Name = "targetWalletCombo";
            this.targetWalletCombo.Size = new System.Drawing.Size(238, 23);
            this.targetWalletCombo.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 15);
            this.label1.TabIndex = 13;
            this.label1.Text = "Portfel docelowy";
            // 
            // entryDateEdit
            // 
            this.entryDateEdit.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.entryDateEdit.Location = new System.Drawing.Point(109, 99);
            this.entryDateEdit.Name = "entryDateEdit";
            this.entryDateEdit.Size = new System.Drawing.Size(120, 23);
            this.entryDateEdit.TabIndex = 4;
            // 
            // amountEdit
            // 
            this.amountEdit.DecimalPlaces = 2;
            this.amountEdit.Location = new System.Drawing.Point(109, 70);
            this.amountEdit.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.amountEdit.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.amountEdit.Name = "amountEdit";
            this.amountEdit.Size = new System.Drawing.Size(120, 23);
            this.amountEdit.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 15);
            this.label6.TabIndex = 16;
            this.label6.Text = "Data transakcji";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 15);
            this.label5.TabIndex = 17;
            this.label5.Text = "Kwota";
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(272, 141);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "Anuluj";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // saveButton
            // 
            this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.saveButton.Location = new System.Drawing.Point(191, 141);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 5;
            this.saveButton.Text = "Zapisz";
            this.saveButton.UseVisualStyleBackColor = true;
            // 
            // TransferForm
            // 
            this.AcceptButton = this.saveButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(359, 176);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.entryDateEdit);
            this.Controls.Add(this.amountEdit);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.targetWalletCombo);
            this.Controls.Add(this.sourceWalletCombo);
            this.Name = "TransferForm";
            this.Text = "Tworzenie nowego transferu";
            ((System.ComponentModel.ISupportInitialize)(this.amountEdit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label2;
        private ComboBox sourceWalletCombo;
        private ComboBox targetWalletCombo;
        private Label label1;
        private DateTimePicker entryDateEdit;
        private NumericUpDown amountEdit;
        private Label label6;
        private Label label5;
        private Button cancelButton;
        private Button saveButton;
    }
}