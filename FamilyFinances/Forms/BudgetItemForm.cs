﻿using FamilyFinances.Entity;

namespace FamilyFinances.Forms
{
    public partial class BudgetItemForm : Form
    {
        public BudgetItemForm()
        {
            InitializeComponent();

            saveButton.Click += SaveButton_Click;
            cancelButton.Click += CancelButton_Click;
        }

        private BudgetItem? _value;

        public BudgetItem Value
        {
            get
            {
                return _value ?? new();
            }
            set
            {
                _value = value;
            }
        }

        public int Count { get; set; } = 1;

        private void SaveButton_Click(object? sender, EventArgs e)
        {
            _value ??= new();

            Value.Description = nameEdit.Text;
            Value.Amount = amountEdit.Value;
            Count = (int)countEdit.Value;

            DialogResult = DialogResult.OK;
        }

        private void CancelButton_Click(object? sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

    }
}
