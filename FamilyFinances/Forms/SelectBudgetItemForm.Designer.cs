﻿namespace FamilyFinances.Forms
{
    partial class SelectBudgetItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            plannerBox = new CheckedListBox();
            cancelButton = new Button();
            saveButton = new Button();
            SuspendLayout();
            // 
            // plannerBox
            // 
            plannerBox.CheckOnClick = true;
            plannerBox.FormattingEnabled = true;
            plannerBox.Items.AddRange(new object[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20" });
            plannerBox.Location = new Point(12, 12);
            plannerBox.MultiColumn = true;
            plannerBox.Name = "plannerBox";
            plannerBox.Size = new Size(266, 418);
            plannerBox.TabIndex = 1;
            // 
            // cancelButton
            // 
            cancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            cancelButton.DialogResult = DialogResult.Cancel;
            cancelButton.Location = new Point(209, 451);
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new Size(75, 23);
            cancelButton.TabIndex = 13;
            cancelButton.Text = "Anuluj";
            cancelButton.UseVisualStyleBackColor = true;
            // 
            // saveButton
            // 
            saveButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            saveButton.DialogResult = DialogResult.OK;
            saveButton.Location = new Point(128, 451);
            saveButton.Name = "saveButton";
            saveButton.Size = new Size(75, 23);
            saveButton.TabIndex = 12;
            saveButton.Text = "Zapisz";
            saveButton.UseVisualStyleBackColor = true;
            // 
            // SelectBudgetItemForm
            // 
            AcceptButton = saveButton;
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            CancelButton = cancelButton;
            ClientSize = new Size(296, 486);
            Controls.Add(cancelButton);
            Controls.Add(saveButton);
            Controls.Add(plannerBox);
            Name = "SelectBudgetItemForm";
            Text = "Wybierz pozycje budżetu";
            ResumeLayout(false);
        }

        #endregion

        private CheckedListBox plannerBox;
        private Button cancelButton;
        private Button saveButton;
    }
}