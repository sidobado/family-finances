﻿using FamilyFinances.Entity;

namespace FamilyFinances.Forms
{
    public partial class CategoryForm : Form, IEntityForm<Category>
    {
        public CategoryForm()
        {
            InitializeComponent();
            cancelButton.Click += CancelButton_Click;
            saveButton.Click += SaveButton_Click;
        }

        private void SaveButton_Click(object? sender, EventArgs e)
        {
            if (Value != null && Value.Name == nameEdit.Text)
            {
                DialogResult = DialogResult.Cancel;
                return;
            }

            if (Value == null)
            {
                _category = new Category();
            }

            Value.Name = nameEdit.Text;
            DialogResult = DialogResult.OK;
        }

        private void CancelButton_Click(object? sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private Category? _category;
        public Category? Value
        {
            get => _category;
            set
            {
                _category = value;
                nameEdit.Text = _category?.Name;
                Text = "Edycja kategorii: " + _category?.Name;
            }
        }
    }
}
