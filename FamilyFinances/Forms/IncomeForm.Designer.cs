﻿namespace FamilyFinances.Forms
{
    partial class IncomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            formPanel = new Panel();
            descriptionEdit = new TextBox();
            entryDateEdit = new DateTimePicker();
            saveButton = new Button();
            addWalletButton = new Button();
            cancelButton = new Button();
            amountEdit = new NumericUpDown();
            entryDateLabel = new Label();
            amountLabel = new Label();
            walletLabel = new Label();
            descriptionLabel = new Label();
            walletCombo = new ComboBox();
            formPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)amountEdit).BeginInit();
            SuspendLayout();
            // 
            // formPanel
            // 
            formPanel.Controls.Add(descriptionEdit);
            formPanel.Controls.Add(entryDateEdit);
            formPanel.Controls.Add(saveButton);
            formPanel.Controls.Add(addWalletButton);
            formPanel.Controls.Add(cancelButton);
            formPanel.Controls.Add(amountEdit);
            formPanel.Controls.Add(entryDateLabel);
            formPanel.Controls.Add(amountLabel);
            formPanel.Controls.Add(walletLabel);
            formPanel.Controls.Add(descriptionLabel);
            formPanel.Controls.Add(walletCombo);
            formPanel.Dock = DockStyle.Fill;
            formPanel.Location = new Point(0, 0);
            formPanel.Margin = new Padding(3, 2, 3, 2);
            formPanel.MinimumSize = new Size(260, 177);
            formPanel.Name = "formPanel";
            formPanel.Size = new Size(325, 185);
            formPanel.TabIndex = 20;
            // 
            // descriptionEdit
            // 
            descriptionEdit.Location = new Point(100, 5);
            descriptionEdit.Name = "descriptionEdit";
            descriptionEdit.Size = new Size(187, 23);
            descriptionEdit.TabIndex = 25;
            // 
            // entryDateEdit
            // 
            entryDateEdit.Format = DateTimePickerFormat.Short;
            entryDateEdit.Location = new Point(100, 93);
            entryDateEdit.Name = "entryDateEdit";
            entryDateEdit.Size = new Size(120, 23);
            entryDateEdit.TabIndex = 22;
            // 
            // saveButton
            // 
            saveButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            saveButton.DialogResult = DialogResult.OK;
            saveButton.Location = new Point(158, 150);
            saveButton.Name = "saveButton";
            saveButton.Size = new Size(75, 23);
            saveButton.TabIndex = 15;
            saveButton.Text = "Zapisz";
            saveButton.UseVisualStyleBackColor = true;
            // 
            // addWalletButton
            // 
            addWalletButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            addWalletButton.Location = new Point(293, 34);
            addWalletButton.Name = "addWalletButton";
            addWalletButton.Size = new Size(20, 23);
            addWalletButton.TabIndex = 19;
            addWalletButton.Text = "+";
            addWalletButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            cancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            cancelButton.DialogResult = DialogResult.Cancel;
            cancelButton.Location = new Point(238, 150);
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new Size(75, 23);
            cancelButton.TabIndex = 16;
            cancelButton.Text = "Anuluj";
            cancelButton.UseVisualStyleBackColor = true;
            // 
            // amountEdit
            // 
            amountEdit.DecimalPlaces = 2;
            amountEdit.Location = new Point(100, 64);
            amountEdit.Maximum = new decimal(new int[] { 100000, 0, 0, 0 });
            amountEdit.Minimum = new decimal(new int[] { 100000, 0, 0, int.MinValue });
            amountEdit.Name = "amountEdit";
            amountEdit.Size = new Size(120, 23);
            amountEdit.TabIndex = 21;
            // 
            // entryDateLabel
            // 
            entryDateLabel.AutoSize = true;
            entryDateLabel.Location = new Point(3, 97);
            entryDateLabel.Name = "entryDateLabel";
            entryDateLabel.Size = new Size(84, 15);
            entryDateLabel.TabIndex = 23;
            entryDateLabel.Text = "Data transakcji";
            // 
            // amountLabel
            // 
            amountLabel.AutoSize = true;
            amountLabel.Location = new Point(3, 66);
            amountLabel.Name = "amountLabel";
            amountLabel.Size = new Size(40, 15);
            amountLabel.TabIndex = 24;
            amountLabel.Text = "Kwota";
            // 
            // walletLabel
            // 
            walletLabel.AutoSize = true;
            walletLabel.Location = new Point(3, 38);
            walletLabel.Name = "walletLabel";
            walletLabel.Size = new Size(42, 15);
            walletLabel.TabIndex = 18;
            walletLabel.Text = "Portfel";
            // 
            // descriptionLabel
            // 
            descriptionLabel.AutoSize = true;
            descriptionLabel.Location = new Point(3, 8);
            descriptionLabel.Name = "descriptionLabel";
            descriptionLabel.Size = new Size(31, 15);
            descriptionLabel.TabIndex = 18;
            descriptionLabel.Text = "Opis";
            // 
            // walletCombo
            // 
            walletCombo.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            walletCombo.FormattingEnabled = true;
            walletCombo.Location = new Point(100, 34);
            walletCombo.Name = "walletCombo";
            walletCombo.Size = new Size(187, 23);
            walletCombo.TabIndex = 13;
            // 
            // IncomeForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(325, 185);
            Controls.Add(formPanel);
            MinimumSize = new Size(278, 224);
            Name = "IncomeForm";
            Text = "Dodawanie nowego przychodu";
            formPanel.ResumeLayout(false);
            formPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)amountEdit).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private Panel formPanel;
        private Button cancelButton;
        private Button saveButton;
        private Label walletLabel;
        private Label descriptionLabel;
        private ComboBox walletCombo;
        private Button addWalletButton;
        private DateTimePicker entryDateEdit;
        private NumericUpDown amountEdit;
        private Label entryDateLabel;
        private Label amountLabel;
        private TextBox descriptionEdit;
    }
}