﻿using FamilyFinances.Entity;

namespace FamilyFinances.Forms
{
    public partial class SelectBudgetItemForm : Form
    {
        private DateTime _entryDate;
        public DateTime EntryDate
        {
            get => _entryDate;
            set
            {
                if (value != _entryDate)
                {
                    _entryDate = value;
                    ReloadPlanner();
                }
            }
        }

        public Budget? Budget { set; get; } = null;
        private BudgetItem? _budgetItem = null;
        public BudgetItem? BudgetItem
        {
            get => _budgetItem;
            set
            {
                if (value != _budgetItem)
                {
                    _budgetItem = value;
                    if (value != null)
                    {
                        foreach (var item in plannerBox.Items)
                        {
                            if (item is BudgetItem obj && obj.Id == value.Id)
                            {
                                int index = plannerBox.Items.IndexOf(item);
                                plannerBox.SetItemChecked(index, true);
                                break;
                            }
                        }
                    }
                }
            }
        }

        private Budget? LastLoadedBudget { get; set; } = null;

        public SelectBudgetItemForm()
        {
            InitializeComponent();

            plannerBox.ItemCheck += PlannerBox_ItemCheck;
        }

        private void ReloadPlanner()
        {
            var budgetRepo = new Repository.Budget();
            var budget = budgetRepo.GetByDate(EntryDate);

            if ((budget?.Id ?? 0) == (LastLoadedBudget?.Id ?? 0))
            {
                return;
            }
            LastLoadedBudget = budget;

            plannerBox.Items.Clear();

            if (budget == null)
            {
                return;
            }

            var budgetItemRepo = new Repository.BudgetItem();
            BudgetItem[] items = budgetItemRepo.GetItems(budget);

            foreach (var item in items)
            {
                plannerBox.Items.Add(item);
            }

        }

        private void PlannerBox_ItemCheck(object? sender, ItemCheckEventArgs e)
        {
            plannerBox.SuspendLayout();
            for (int i = 0; i < plannerBox.Items.Count; i++)
            {
                var item = plannerBox.Items[i];
                if (item != plannerBox.Items[e.Index])
                {
                    plannerBox.SetItemChecked(plannerBox.Items.IndexOf(item), false);
                }
            }
            saveButton.Enabled = e.NewValue == CheckState.Checked;
            if (saveButton.Enabled)
            {
                BudgetItem = (BudgetItem)plannerBox.Items[e.Index];
            }
            plannerBox.ResumeLayout();
        }
    }
}
