﻿using FamilyFinances.Entity;
using ZefirLib.ORM;

namespace FamilyFinances.Forms
{
    public partial class ProductForm : Form, IEntityForm<Product>
    {
        public ProductForm()
        {
            InitializeComponent();
            FillCombo();
            cancelButton.Click += CancelButton_Click;
            saveButton.Click += SaveButton_Click;
        }

        private void FillCombo()
        {
            using var repo = new Repository<Unit>();
            var units = repo.GetAll("name");
            foreach (var unit in units)
            {
                unitCombo.Items.Add(unit);
            }
        }

        private void SaveButton_Click(object? sender, EventArgs e)
        {
            if (Value == null)
            {
                _product = new Product();
            }

            Value.Name = nameEdit.Text;
            Value.CategoryId = categoryMultiSelect1.GetSelected();
            Value.UnitId = (unitCombo.SelectedItem as Unit).Id;

            DialogResult = DialogResult.OK;
        }

        private void CancelButton_Click(object? sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private Product? _product;
        public Product? Value
        {
            get => _product;
            set
            {
                _product = value;
                nameEdit.Text = _product?.Name;
                foreach (var item in unitCombo.Items)
                {
                    if (item is Unit unit && unit.Id == _product.UnitId)
                    {
                        unitCombo.SelectedItem = item;
                    }
                }

                categoryMultiSelect1.SetSelected(_product.CategoryId);

                Text = "Edycja produktu: " + _product?.Name;
            }
        }
    }
}
