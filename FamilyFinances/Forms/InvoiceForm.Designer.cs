﻿using FamilyFinances.Control;

namespace FamilyFinances.Forms
{
    partial class InvoiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            mainPanel = new GroupBox();
            dgv = new PopupDataGridView();
            cancelButton = new Button();
            saveButton = new Button();
            bottomPanel = new Panel();
            budgetNameLabel = new Label();
            panel3 = new Panel();
            topPanel = new GroupBox();
            changeBudgetButton = new Button();
            panel2 = new Panel();
            panel1 = new Panel();
            label7 = new Label();
            addPlaceButton = new Button();
            addWalletButton = new Button();
            inBudgetCheck = new CheckBox();
            label1 = new Label();
            placeCombo = new ComboBox();
            walletCombo = new ComboBox();
            entryDateEdit = new DateTimePicker();
            label6 = new Label();
            label2 = new Label();
            Id = new DataGridViewTextBoxColumn();
            Product = new DataGridViewTextBoxColumn();
            Quantity = new DataGridViewTextBoxColumn();
            Amount = new DataGridViewTextBoxColumn();
            PerUnit = new DataGridViewTextBoxColumn();
            mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dgv).BeginInit();
            bottomPanel.SuspendLayout();
            topPanel.SuspendLayout();
            SuspendLayout();
            // 
            // mainPanel
            // 
            mainPanel.Controls.Add(dgv);
            mainPanel.Dock = DockStyle.Fill;
            mainPanel.Location = new Point(0, 76);
            mainPanel.Name = "mainPanel";
            mainPanel.Size = new Size(890, 383);
            mainPanel.TabIndex = 1;
            mainPanel.TabStop = false;
            mainPanel.Text = "Pozycje rachunku";
            // 
            // dgv
            // 
            dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgv.Columns.AddRange(new DataGridViewColumn[] { Id, Product, Quantity, Amount, PerUnit });
            dgv.Dock = DockStyle.Fill;
            dgv.Location = new Point(3, 19);
            dgv.Name = "dgv";
            dgv.RowTemplate.Height = 25;
            dgv.ShowCellErrors = false;
            dgv.ShowCellToolTips = false;
            dgv.ShowRowErrors = false;
            dgv.Size = new Size(884, 361);
            dgv.TabIndex = 0;
            // 
            // cancelButton
            // 
            cancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            cancelButton.DialogResult = DialogResult.Cancel;
            cancelButton.Location = new Point(803, 4);
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new Size(75, 23);
            cancelButton.TabIndex = 1;
            cancelButton.Text = "Anuluj";
            cancelButton.UseVisualStyleBackColor = true;
            // 
            // saveButton
            // 
            saveButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            saveButton.DialogResult = DialogResult.OK;
            saveButton.Enabled = false;
            saveButton.Location = new Point(722, 4);
            saveButton.Name = "saveButton";
            saveButton.Size = new Size(75, 23);
            saveButton.TabIndex = 0;
            saveButton.Text = "Zapisz";
            saveButton.UseVisualStyleBackColor = true;
            // 
            // bottomPanel
            // 
            bottomPanel.Controls.Add(cancelButton);
            bottomPanel.Controls.Add(saveButton);
            bottomPanel.Dock = DockStyle.Bottom;
            bottomPanel.Location = new Point(0, 459);
            bottomPanel.Name = "bottomPanel";
            bottomPanel.Size = new Size(890, 30);
            bottomPanel.TabIndex = 2;
            // 
            // budgetNameLabel
            // 
            budgetNameLabel.AutoSize = true;
            budgetNameLabel.Location = new Point(695, 40);
            budgetNameLabel.Name = "budgetNameLabel";
            budgetNameLabel.Size = new Size(0, 15);
            budgetNameLabel.TabIndex = 23;
            // 
            // panel3
            // 
            panel3.Location = new Point(616, 37);
            panel3.Name = "panel3";
            panel3.Size = new Size(14, 23);
            panel3.TabIndex = 22;
            // 
            // topPanel
            // 
            topPanel.Controls.Add(budgetNameLabel);
            topPanel.Controls.Add(panel3);
            topPanel.Controls.Add(changeBudgetButton);
            topPanel.Controls.Add(panel2);
            topPanel.Controls.Add(panel1);
            topPanel.Controls.Add(label7);
            topPanel.Controls.Add(addPlaceButton);
            topPanel.Controls.Add(addWalletButton);
            topPanel.Controls.Add(inBudgetCheck);
            topPanel.Controls.Add(label1);
            topPanel.Controls.Add(placeCombo);
            topPanel.Controls.Add(walletCombo);
            topPanel.Controls.Add(entryDateEdit);
            topPanel.Controls.Add(label6);
            topPanel.Controls.Add(label2);
            topPanel.Dock = DockStyle.Top;
            topPanel.Location = new Point(0, 0);
            topPanel.Name = "topPanel";
            topPanel.Size = new Size(890, 76);
            topPanel.TabIndex = 0;
            topPanel.TabStop = false;
            topPanel.Text = "Rachunek";
            // 
            // changeBudgetButton
            // 
            changeBudgetButton.Enabled = false;
            changeBudgetButton.FlatStyle = FlatStyle.Popup;
            changeBudgetButton.Location = new Point(636, 37);
            changeBudgetButton.Name = "changeBudgetButton";
            changeBudgetButton.Size = new Size(52, 23);
            changeBudgetButton.TabIndex = 9;
            changeBudgetButton.Text = "zmień";
            changeBudgetButton.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            panel2.Location = new Point(374, 37);
            panel2.Name = "panel2";
            panel2.Size = new Size(14, 23);
            panel2.TabIndex = 22;
            // 
            // panel1
            // 
            panel1.Location = new Point(132, 37);
            panel1.Name = "panel1";
            panel1.Size = new Size(14, 23);
            panel1.TabIndex = 22;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new Point(657, 19);
            label7.Name = "label7";
            label7.Size = new Size(68, 15);
            label7.TabIndex = 20;
            label7.Text = "W budżecie";
            // 
            // addPlaceButton
            // 
            addPlaceButton.Location = new Point(587, 37);
            addPlaceButton.Name = "addPlaceButton";
            addPlaceButton.Size = new Size(23, 23);
            addPlaceButton.TabIndex = 7;
            addPlaceButton.Text = "+";
            addPlaceButton.UseVisualStyleBackColor = true;
            // 
            // addWalletButton
            // 
            addWalletButton.Location = new Point(345, 37);
            addWalletButton.Name = "addWalletButton";
            addWalletButton.Size = new Size(23, 23);
            addWalletButton.TabIndex = 4;
            addWalletButton.Text = "+";
            addWalletButton.UseVisualStyleBackColor = true;
            // 
            // inBudgetCheck
            // 
            inBudgetCheck.AutoSize = true;
            inBudgetCheck.Location = new Point(636, 20);
            inBudgetCheck.Name = "inBudgetCheck";
            inBudgetCheck.Size = new Size(15, 14);
            inBudgetCheck.TabIndex = 8;
            inBudgetCheck.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(394, 19);
            label1.Name = "label1";
            label1.Size = new Size(47, 15);
            label1.TabIndex = 5;
            label1.Text = "Miejsce";
            // 
            // placeCombo
            // 
            placeCombo.FormattingEnabled = true;
            placeCombo.Location = new Point(394, 37);
            placeCombo.Name = "placeCombo";
            placeCombo.Size = new Size(187, 23);
            placeCombo.TabIndex = 6;
            // 
            // walletCombo
            // 
            walletCombo.FormattingEnabled = true;
            walletCombo.Location = new Point(152, 37);
            walletCombo.Name = "walletCombo";
            walletCombo.Size = new Size(187, 23);
            walletCombo.TabIndex = 3;
            // 
            // entryDateEdit
            // 
            entryDateEdit.Format = DateTimePickerFormat.Short;
            entryDateEdit.Location = new Point(6, 37);
            entryDateEdit.Name = "entryDateEdit";
            entryDateEdit.Size = new Size(120, 23);
            entryDateEdit.TabIndex = 1;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(6, 19);
            label6.Name = "label6";
            label6.Size = new Size(84, 15);
            label6.TabIndex = 0;
            label6.Text = "Data transakcji";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(152, 19);
            label2.Name = "label2";
            label2.Size = new Size(42, 15);
            label2.TabIndex = 2;
            label2.Text = "Portfel";
            // 
            // Id
            // 
            Id.HeaderText = "Id";
            Id.Name = "Id";
            Id.ReadOnly = true;
            Id.Width = 60;
            // 
            // Product
            // 
            Product.HeaderText = "Produkt";
            Product.Name = "Product";
            Product.Width = 350;
            // 
            // Quantity
            // 
            Quantity.HeaderText = "Ilość";
            Quantity.Name = "Quantity";
            Quantity.Width = 50;
            // 
            // Amount
            // 
            Amount.HeaderText = "Kwota";
            Amount.Name = "Amount";
            Amount.Width = 50;
            // 
            // PerUnit
            // 
            PerUnit.HeaderText = "Na jednostke";
            PerUnit.Name = "PerUnit";
            PerUnit.ReadOnly = true;
            // 
            // InvoiceForm
            // 
            AcceptButton = saveButton;
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            CancelButton = cancelButton;
            ClientSize = new Size(890, 489);
            Controls.Add(mainPanel);
            Controls.Add(bottomPanel);
            Controls.Add(topPanel);
            Name = "InvoiceForm";
            Text = "Dodawanie paragonu";
            mainPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dgv).EndInit();
            bottomPanel.ResumeLayout(false);
            topPanel.ResumeLayout(false);
            topPanel.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private GroupBox mainPanel;
        private PopupDataGridView dgv;
        private Button cancelButton;
        private Button saveButton;
        private Panel bottomPanel;
        private Label budgetNameLabel;
        private Panel panel3;
        private GroupBox topPanel;
        private Button changeBudgetButton;
        private Panel panel2;
        private Panel panel1;
        private Label label7;
        private Button addPlaceButton;
        private Button addWalletButton;
        private CheckBox inBudgetCheck;
        private Label label1;
        private ComboBox placeCombo;
        private ComboBox walletCombo;
        private DateTimePicker entryDateEdit;
        private Label label6;
        private Label label2;
        private DataGridViewTextBoxColumn Id;
        private DataGridViewTextBoxColumn Product;
        private DataGridViewTextBoxColumn Quantity;
        private DataGridViewTextBoxColumn Amount;
        private DataGridViewTextBoxColumn PerUnit;
    }
}