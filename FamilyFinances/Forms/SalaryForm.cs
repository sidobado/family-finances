﻿using FamilyFinances.Entity;

namespace FamilyFinances.Forms
{
    public partial class SalaryForm : Form, IEntityForm<Salary>
    {
        public SalaryForm()
        {
            InitializeComponent();

            cancelButton.Click += CancelButton_Click;
            saveButton.Click += SaveButton_Click;
        }

        private Salary? _salary;
        public Salary? Value
        {
            get => _salary;
            set
            {
                _salary = value;
                if (value != null)
                {
                    nameEdit.Text = value.Name;
                    amountEdit.Value = value.Amount;
                }
                Text = "Edycja wynagrodzenia";
            }
        }

        private void SaveButton_Click(object? sender, EventArgs e)
        {
            if (Value == null)
            {
                _salary = new();
            }

            Value.Name = nameEdit.Text;
            Value.Amount = amountEdit.Value;

            DialogResult = DialogResult.OK;
        }

        private void CancelButton_Click(object? sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
