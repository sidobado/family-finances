﻿namespace FamilyFinances.Forms
{
    partial class BudgetItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            nameEdit = new TextBox();
            amountEdit = new NumericUpDown();
            label5 = new Label();
            label2 = new Label();
            countEdit = new NumericUpDown();
            cancelButton = new Button();
            saveButton = new Button();
            ((System.ComponentModel.ISupportInitialize)amountEdit).BeginInit();
            ((System.ComponentModel.ISupportInitialize)countEdit).BeginInit();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(12, 15);
            label1.Name = "label1";
            label1.Size = new Size(31, 15);
            label1.TabIndex = 0;
            label1.Text = "Opis";
            // 
            // nameEdit
            // 
            nameEdit.Location = new Point(109, 12);
            nameEdit.Name = "nameEdit";
            nameEdit.Size = new Size(238, 23);
            nameEdit.TabIndex = 1;
            // 
            // amountEdit
            // 
            amountEdit.DecimalPlaces = 2;
            amountEdit.Location = new Point(109, 41);
            amountEdit.Maximum = new decimal(new int[] { 100000, 0, 0, 0 });
            amountEdit.Minimum = new decimal(new int[] { 100000, 0, 0, int.MinValue });
            amountEdit.Name = "amountEdit";
            amountEdit.Size = new Size(120, 23);
            amountEdit.TabIndex = 3;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(12, 43);
            label5.Name = "label5";
            label5.Size = new Size(40, 15);
            label5.TabIndex = 2;
            label5.Text = "Kwota";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(12, 72);
            label2.Name = "label2";
            label2.Size = new Size(90, 15);
            label2.TabIndex = 4;
            label2.Text = "Ilość powtórzeń";
            // 
            // countEdit
            // 
            countEdit.Location = new Point(109, 70);
            countEdit.Maximum = new decimal(new int[] { 12, 0, 0, 0 });
            countEdit.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            countEdit.Name = "countEdit";
            countEdit.Size = new Size(120, 23);
            countEdit.TabIndex = 5;
            countEdit.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // cancelButton
            // 
            cancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            cancelButton.DialogResult = DialogResult.Cancel;
            cancelButton.Location = new Point(279, 119);
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new Size(75, 23);
            cancelButton.TabIndex = 7;
            cancelButton.Text = "Anuluj";
            cancelButton.UseVisualStyleBackColor = true;
            // 
            // saveButton
            // 
            saveButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            saveButton.DialogResult = DialogResult.OK;
            saveButton.Location = new Point(198, 119);
            saveButton.Name = "saveButton";
            saveButton.Size = new Size(75, 23);
            saveButton.TabIndex = 6;
            saveButton.Text = "Zapisz";
            saveButton.UseVisualStyleBackColor = true;
            // 
            // BudgetItemForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(366, 154);
            Controls.Add(cancelButton);
            Controls.Add(saveButton);
            Controls.Add(countEdit);
            Controls.Add(label2);
            Controls.Add(amountEdit);
            Controls.Add(label5);
            Controls.Add(label1);
            Controls.Add(nameEdit);
            Name = "BudgetItemForm";
            Text = "Dodawanie wydatku do planu";
            ((System.ComponentModel.ISupportInitialize)amountEdit).EndInit();
            ((System.ComponentModel.ISupportInitialize)countEdit).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private TextBox nameEdit;
        private NumericUpDown amountEdit;
        private Label label5;
        private Label label2;
        private NumericUpDown countEdit;
        private Button cancelButton;
        private Button saveButton;
    }
}