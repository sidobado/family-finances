﻿using FamilyFinances.Entity;
using ZefirLib.ORM;

namespace FamilyFinances.Forms
{
    public partial class IncomeForm : Form, IEntityForm<Income>
    {
        public IncomeForm()
        {
            InitializeComponent();
            Helper.FillCombo<Wallet>(walletCombo, "name");

            addWalletButton.Click += AddWalletButton_Click;

            cancelButton.Click += CancelButton_Click;
            saveButton.Click += SaveButton_Click;

            Load += IncomeForm_Load;
        }

        private void IncomeForm_Load(object? sender, EventArgs e)
        {
            amountEdit.Select();
        }

        private Income? _income;
        public Income? Value
        {
            get => _income;
            set
            {
                _income = value;
                if (value != null)
                {
                    descriptionEdit.Text = value.Description;
                    Helper.SelectComboItem(walletCombo, value.WalletId);
                    amountEdit.Value = value.Amount;
                    entryDateEdit.Value = value.EntryDate;
                    Text = "Edycja przychodu";
                }
            }
        }

        private void AddWalletButton_Click(object? sender, EventArgs e)
        {
            using var form = new WalletForm();
            if (form.ShowDialog() == DialogResult.OK && form.Value != null)
            {
                using var repo = new Repository<Wallet>();
                var entity = repo.Save(form.Value);
                Helper.FillCombo<Wallet>(walletCombo, "name");
                Helper.SelectComboItem(walletCombo, entity);
            }
        }

        private void SaveButton_Click(object? sender, EventArgs e)
        {
            if (Value == null)
            {
                _income = new();
            }

            Value.Description = descriptionEdit.Text;
            Value.WalletId = (walletCombo.SelectedItem as Wallet).Id;
            Value.Amount = amountEdit.Value;
            Value.EntryDate = entryDateEdit.Value;

            DialogResult = DialogResult.OK;
        }

        private void CancelButton_Click(object? sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
