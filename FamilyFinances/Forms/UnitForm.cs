﻿using FamilyFinances.Entity;

namespace FamilyFinances.Forms
{
    public partial class UnitForm : Form, IEntityForm<Unit>
    {
        public UnitForm()
        {
            InitializeComponent();
            cancelButton.Click += CancelButton_Click;
            saveButton.Click += SaveButton_Click;
        }

        private void SaveButton_Click(object? sender, EventArgs e)
        {
            if (Value != null && Value.Name == nameEdit.Text)
            {
                DialogResult = DialogResult.Cancel;
                return;
            }

            if (Value == null)
            {
                _unit = new Unit();
            }

            Value.Name = nameEdit.Text;
            DialogResult = DialogResult.OK;
        }

        private void CancelButton_Click(object? sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private Unit? _unit;
        public Unit? Value
        {
            get => _unit;
            set
            {
                _unit = value;
                nameEdit.Text = _unit?.Name;
                Text = "Edycja jednostki: " + _unit?.Name;
            }
        }
    }
}
