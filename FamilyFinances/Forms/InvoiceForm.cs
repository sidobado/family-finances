﻿using FamilyFinances.Data;
using FamilyFinances.DTO;
using FamilyFinances.Entity;
using ZefirLib.ORM;

namespace FamilyFinances.Forms
{
    public partial class InvoiceForm : Form
    {
        private Transaction[] Transactions { get; set; } = Array.Empty<Transaction>();
        private BudgetItem? BudgetItem { get; set; }

        public InvoiceForm()
        {
            InitializeComponent();
            if (Helper.IsDesignMode)
            {
                return;
            }

            FillCombos();
            SetupPopup();

            dgv.CellValueChanged += Dgv_CellValueChanged;
            changeBudgetButton.Click += ChangeBudgetButton_Click;
            inBudgetCheck.CheckedChanged += InBudgetCheck_CheckedChanged;
            saveButton.Click += SaveButton_Click;
            walletCombo.SelectedIndexChanged += Combo_SelectedIndexChanged;
            placeCombo.SelectedIndexChanged += Combo_SelectedIndexChanged;
            addWalletButton.Click += AddWalletButton_Click;
            addPlaceButton.Click += AddPlaceButton_Click;
        }

        private void FillCombos()
        {
            Helper.FillCombo<Wallet>(walletCombo, "id");
            Helper.FillCombo<Place>(placeCombo, "id");
        }

        private void SetupPopup()
        {
            dgv.AddPopup("Product", new ProductConnector());
        }

        public void LoadInvoice(InvoiceDTO invoiceDTO)
        {
            entryDateEdit.Value = invoiceDTO.Date;
            Helper.SelectComboItem(walletCombo, invoiceDTO.Wallet.Id);
            Helper.SelectComboItem(placeCombo, invoiceDTO.Place.Id);
            inBudgetCheck.Checked = invoiceDTO.BudgetName.Length > 0;
            changeBudgetButton.Enabled = inBudgetCheck.Checked;
            budgetNameLabel.Text = invoiceDTO.BudgetName;

            var productRepo = new Repository<Product>();
            var transactionsRepo = new Repository.Transaction();
            var unitRepo = new Repository<Unit>();

            Transactions = transactionsRepo.GetAllByIds(invoiceDTO.TransactionIds);
            if (Transactions.Length > 0)
            {
                int? budgetItemId = Transactions[0].BudgetItemId;
                if (budgetItemId != null)
                {
                    var budgetItemRepo = new Repository.BudgetItem();
                    BudgetItem = budgetItemRepo.Get((int)budgetItemId);
                }
            }

            dgv.Rows.Clear();
            foreach (var item in Transactions)
            {
                var product = productRepo.Get(item.ProductId);
                int index = dgv.Rows.Add();
                dgv.Rows[index].Tag = item;
                dgv.Rows[index].Cells["Id"].Value = item.Id;
                dgv.Rows[index].Cells["Product"].Value = product;
                dgv.Rows[index].Cells["Quantity"].Value = item.Quantity.ToString();
                dgv.Rows[index].Cells["Amount"].Value = item.Amount.ToString("N2");
                dgv.Rows[index].Cells["PerUnit"].Value = (item.Amount / item.Quantity).ToString("N2") + "/" + unitRepo.Get(product.UnitId).Name;
            }

            CalculateTotalPrice();
            ValidateForm();
            Text = "Edycja paragonu";
        }

        private void Dgv_CellValueChanged(object? sender, DataGridViewCellEventArgs e)
        {
            CalculateTotalPrice();
            ValidateForm();

            if (e.ColumnIndex != 1 && e.ColumnIndex != 2 && e.ColumnIndex != 3)
            {
                return;
            }

            if (dgv.Rows[e.RowIndex].Cells["Product"].Value is Product product)
            {
                var repo = new Repository<Unit>();
                bool p1 = decimal.TryParse(dgv.Rows[e.RowIndex].Cells["Amount"].Value?.ToString(), out decimal price);
                bool p2 = decimal.TryParse(dgv.Rows[e.RowIndex].Cells["Quantity"].Value?.ToString(), out decimal quantity);
                if (p1 && p2 && quantity > 0)
                {
                    dgv.Rows[e.RowIndex].Cells["PerUnit"].Value = (price / quantity).ToString("N2") + "/" + repo.Get(product.UnitId).Name;
                }
            }
        }

        private void CalculateTotalPrice()
        {
            decimal totalPrice = 0;
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                bool p = decimal.TryParse(dgv.Rows[i].Cells["Amount"].Value?.ToString(), out decimal price);
                if (p) { totalPrice += price; }
            }

            topPanel.Text = "Rachunek: " + totalPrice.ToString("N2");
        }

        private void ValidateForm()
        {
            bool isValid = true;
            if (dgv.Rows.Count == 1 && dgv.Rows[0].IsNewRow) { isValid = false; }
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                if (dgv.Rows[i].IsNewRow) { continue; }
                bool p1 = decimal.TryParse(dgv.Rows[i].Cells["Amount"].Value?.ToString(), out decimal price);
                bool p2 = decimal.TryParse(dgv.Rows[i].Cells["Quantity"].Value?.ToString(), out decimal quantity);
                object? product = dgv.Rows[i].Cells["Product"].Value;
                if (product == null) { isValid = false; break; }
                if (product is not Entity.Product) { isValid = false; break; }

                if (!p1 || !p2 || price <= 0 || quantity <= 0)
                {
                    isValid = false;
                    break;
                }
            }

            if (walletCombo.SelectedIndex == -1) { isValid = false; }
            if (placeCombo.SelectedIndex == -1) { isValid = false; }
            if (inBudgetCheck.Checked && BudgetItem is null) { isValid = false; }

            saveButton.Enabled = isValid;
        }

        private void SaveButton_Click(object? sender, EventArgs e)
        {
            UpdateChanged();
            InsertNew();
        }

        private void Combo_SelectedIndexChanged(object? sender, EventArgs e)
        {
            ValidateForm();
        }

        private void AddWalletButton_Click(object? sender, EventArgs e)
        {
            using var form = new WalletForm();
            if (form.ShowDialog() == DialogResult.OK && form.Value != null)
            {
                using var repo = new Repository<Wallet>();
                var entity = repo.Save(form.Value);
                Helper.FillCombo<Wallet>(walletCombo, "name");
                Helper.SelectComboItem(walletCombo, entity);
            }
        }

        private void AddPlaceButton_Click(object? sender, EventArgs e)
        {
            using var form = new PlaceForm();
            if (form.ShowDialog() == DialogResult.OK && form.Value != null)
            {
                using var repo = new Repository<Place>();
                var entity = repo.Save(form.Value);
                Helper.FillCombo<Place>(placeCombo, "name");
                Helper.SelectComboItem(placeCombo, entity);
            }
        }

        private void ChangeBudgetButton_Click(object? sender, EventArgs e)
        {
            var form = new SelectBudgetItemForm
            {
                EntryDate = entryDateEdit.Value,
                BudgetItem = BudgetItem
            };
            var result = form.ShowDialog();
            if (result == DialogResult.OK)
            {
                BudgetItem = form.BudgetItem;
                budgetNameLabel.Text = BudgetItem?.ToString() ?? string.Empty;
            }

            ValidateForm();
        }

        private void InBudgetCheck_CheckedChanged(object? sender, EventArgs e)
        {
            if (inBudgetCheck.Checked)
            {
                changeBudgetButton.Enabled = true;
                budgetNameLabel.Text = BudgetItem?.ToString() ?? string.Empty;
            }
            else
            {
                changeBudgetButton.Enabled = false;
                budgetNameLabel.Text = string.Empty;
            }

            ValidateForm();
        }

        private void UpdateChanged()
        {
            var repo = new Repository<Transaction>();
            foreach (var transaction in Transactions)
            {
                int rowIndex = FindTransactionRow(transaction);
                if (rowIndex == -1)
                {
                    repo.Delete(transaction);
                }
                else
                {
                    transaction.ProductId = (dgv.Rows[rowIndex].Cells["Product"].Value as Product).Id;
                    transaction.WalletId = (walletCombo.SelectedItem as Wallet).Id;
                    transaction.PlaceId = (placeCombo.SelectedItem as Place).Id;
                    transaction.IsInBudget = inBudgetCheck.Checked;
                    transaction.BudgetItemId = transaction.IsInBudget ? BudgetItem.Id : null;
                    transaction.Amount = decimal.Parse(dgv.Rows[rowIndex].Cells["Amount"].Value.ToString());
                    transaction.EntryDate = entryDateEdit.Value;
                    transaction.Quantity = decimal.Parse(dgv.Rows[rowIndex].Cells["Quantity"].Value.ToString());
                    repo.Save(transaction);
                }
            }
        }

        private int FindTransactionRow(Transaction transaction)
        {
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                bool p = int.TryParse(dgv.Rows[i].Cells["Id"].Value?.ToString(), out int id);
                if (p && id == transaction.Id)
                {
                    return i;
                }
            }
            return -1;
        }

        private void InsertNew()
        {
            var repo = new Repository<Transaction>();
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                var tid = dgv.Rows[i].Cells["Id"].Value?.ToString() ?? string.Empty;
                if (tid != string.Empty || dgv.Rows[i].IsNewRow)
                {
                    continue;
                }
                int rowIndex = i;
                var transaction = new Transaction();
                transaction.ProductId = (dgv.Rows[rowIndex].Cells["Product"].Value as Product).Id;
                transaction.WalletId = (walletCombo.SelectedItem as Wallet).Id;
                transaction.PlaceId = (placeCombo.SelectedItem as Place).Id;
                transaction.IsInBudget = inBudgetCheck.Checked;
                transaction.BudgetItemId = transaction.IsInBudget ? BudgetItem.Id : null;
                transaction.Amount = decimal.Parse(dgv.Rows[rowIndex].Cells["Amount"].Value.ToString());
                transaction.EntryDate = entryDateEdit.Value;
                transaction.Quantity = decimal.Parse(dgv.Rows[rowIndex].Cells["Quantity"].Value.ToString());
                repo.Save(transaction);
            }
        }


    }
}
