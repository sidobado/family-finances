﻿namespace FamilyFinances.Forms
{
    partial class SalaryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            formPanel = new Panel();
            nameEdit = new TextBox();
            saveButton = new Button();
            cancelButton = new Button();
            amountEdit = new NumericUpDown();
            amountLabel = new Label();
            descriptionLabel = new Label();
            formPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)amountEdit).BeginInit();
            SuspendLayout();
            // 
            // formPanel
            // 
            formPanel.Controls.Add(nameEdit);
            formPanel.Controls.Add(saveButton);
            formPanel.Controls.Add(cancelButton);
            formPanel.Controls.Add(amountEdit);
            formPanel.Controls.Add(amountLabel);
            formPanel.Controls.Add(descriptionLabel);
            formPanel.Dock = DockStyle.Fill;
            formPanel.Location = new Point(0, 0);
            formPanel.Margin = new Padding(3, 2, 3, 2);
            formPanel.Name = "formPanel";
            formPanel.Size = new Size(322, 147);
            formPanel.TabIndex = 21;
            // 
            // nameEdit
            // 
            nameEdit.Location = new Point(100, 5);
            nameEdit.Name = "nameEdit";
            nameEdit.Size = new Size(210, 23);
            nameEdit.TabIndex = 25;
            // 
            // saveButton
            // 
            saveButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            saveButton.DialogResult = DialogResult.OK;
            saveButton.Location = new Point(154, 112);
            saveButton.Name = "saveButton";
            saveButton.Size = new Size(75, 23);
            saveButton.TabIndex = 15;
            saveButton.Text = "Zapisz";
            saveButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            cancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            cancelButton.DialogResult = DialogResult.Cancel;
            cancelButton.Location = new Point(235, 112);
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new Size(75, 23);
            cancelButton.TabIndex = 16;
            cancelButton.Text = "Anuluj";
            cancelButton.UseVisualStyleBackColor = true;
            // 
            // amountEdit
            // 
            amountEdit.DecimalPlaces = 2;
            amountEdit.Location = new Point(100, 34);
            amountEdit.Maximum = new decimal(new int[] { 100000, 0, 0, 0 });
            amountEdit.Minimum = new decimal(new int[] { 100000, 0, 0, int.MinValue });
            amountEdit.Name = "amountEdit";
            amountEdit.Size = new Size(120, 23);
            amountEdit.TabIndex = 21;
            // 
            // amountLabel
            // 
            amountLabel.AutoSize = true;
            amountLabel.Location = new Point(3, 36);
            amountLabel.Name = "amountLabel";
            amountLabel.Size = new Size(40, 15);
            amountLabel.TabIndex = 24;
            amountLabel.Text = "Kwota";
            // 
            // descriptionLabel
            // 
            descriptionLabel.AutoSize = true;
            descriptionLabel.Location = new Point(3, 8);
            descriptionLabel.Name = "descriptionLabel";
            descriptionLabel.Size = new Size(31, 15);
            descriptionLabel.TabIndex = 18;
            descriptionLabel.Text = "Opis";
            // 
            // SalaryForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(322, 147);
            Controls.Add(formPanel);
            Name = "SalaryForm";
            Text = "Dodawanie nowego wynagrodzenia";
            formPanel.ResumeLayout(false);
            formPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)amountEdit).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private Panel formPanel;
        private TextBox nameEdit;
        private Button saveButton;
        private Button cancelButton;
        private NumericUpDown amountEdit;
        private Label amountLabel;
        private Label descriptionLabel;
    }
}