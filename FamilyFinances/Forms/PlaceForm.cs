﻿using FamilyFinances.Entity;

namespace FamilyFinances.Forms
{
    public partial class PlaceForm : Form, IEntityForm<Place>
    {
        public PlaceForm()
        {
            InitializeComponent();
            cancelButton.Click += CancelButton_Click;
            saveButton.Click += SaveButton_Click;
        }

        private void SaveButton_Click(object? sender, EventArgs e)
        {
            if (Value != null && Value.Name == nameEdit.Text)
            {
                DialogResult = DialogResult.Cancel;
                return;
            }

            if (Value == null)
            {
                _place = new Place();
            }

            Value.Name = nameEdit.Text;
            DialogResult = DialogResult.OK;
        }

        private void CancelButton_Click(object? sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private Place? _place;
        public Place? Value
        {
            get => _place;
            set
            {
                _place = value;
                nameEdit.Text = _place?.Name;
                Text = "Edycja miejsca: " + _place?.Name;
            }
        }
    }
}
