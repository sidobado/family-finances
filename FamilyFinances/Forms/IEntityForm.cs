﻿namespace FamilyFinances.Forms
{
    public interface IEntityForm<Entity>
    {
        public Entity? Value { get; set; }
    }
}
