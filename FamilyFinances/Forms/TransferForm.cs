﻿using FamilyFinances.Entity;
using ZefirLib.ORM;

namespace FamilyFinances.Forms
{
    public partial class TransferForm : Form, IEntityForm<Transfer>
    {
        public TransferForm()
        {
            InitializeComponent();
            FillCombos();
            cancelButton.Click += CancelButton_Click;
            saveButton.Click += SaveButton_Click;
        }

        private void FillCombos()
        {
            using var walletRepo = new Repository<Wallet>();

            foreach (var item in walletRepo.GetAll("name"))
            {
                sourceWalletCombo.Items.Add(item);
                targetWalletCombo.Items.Add(item);
            }
        }

        private void SaveButton_Click(object? sender, EventArgs e)
        {
            if (Value == null)
            {
                _transfer = new Transfer();
            }

            Value.SourceWalletId = (sourceWalletCombo.SelectedItem as Wallet).Id;
            Value.TargetWalletId = (targetWalletCombo.SelectedItem as Wallet).Id;
            Value.Amount = amountEdit.Value;
            Value.EntryDate = entryDateEdit.Value;

            DialogResult = DialogResult.OK;
        }

        private void CancelButton_Click(object? sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private Transfer? _transfer;
        public Transfer? Value
        {
            get => _transfer;
            set
            {
                _transfer = value;
                foreach (var item in sourceWalletCombo.Items)
                {
                    if (item is Wallet wallet && wallet.Id == value.SourceWalletId)
                    {
                        sourceWalletCombo.SelectedItem = item;
                        break;
                    }
                }
                foreach (var item in targetWalletCombo.Items)
                {
                    if (item is Wallet wallet && wallet.Id == value.TargetWalletId)
                    {
                        targetWalletCombo.SelectedItem = item;
                        break;
                    }
                }

                amountEdit.Value = value.Amount;
                entryDateEdit.Value = value.EntryDate;

                Text = "Edycja transferu";
            }
        }
    }
}
