﻿using ZefirLib.ORM;

namespace FamilyFinances.Sql
{
    public class Migration
    {
        public void Reinstall()
        {
            string filename = "Sql/version_" + Helper.GetApplicationVersion().ToString() + "_reinstall.psql";
            var importer = new SqlImporter(filename);
            importer.Import();
            var _ = Settings.Instance.DatabaseVersion;
            Settings.Instance.DatabaseVersion = Helper.GetApplicationVersion();
        }

        public void Migrate()
        {
            if (false == Database.IsTableExists("setting"))
            {
                Reinstall();
                return;
            }

            int installedVersion = Settings.Instance.DatabaseVersion;
            int applicationVersion = Helper.GetApplicationVersion();

            for (int i = installedVersion + 1; i <= applicationVersion; i++)
            {
                string filename = "Sql/version_" + i.ToString() + ".psql";
                var importer = new SqlImporter(filename);
                importer.Import();
                Settings.Instance.DatabaseVersion = i;
            }
        }

        private class SqlImporter
        {
            private string Filename;
            private StreamReader? Reader;

            public SqlImporter(string filename)
            {
                Filename = filename;
            }

            public void Import()
            {
                try
                {
                    OpenFileStep();
                    string query = "";
                    do
                    {
                        query = ReadQueryStep();
                        if (query.EndsWith(";"))
                        {
                            string connString = Helper.GetDatabaseConnectionString();
                            using var conn = new Npgsql.NpgsqlConnection(Helper.GetDatabaseConnectionString());
                            conn.Open();
                            using var command = conn.CreateCommand();
                            command.CommandText = query;
                            command.ExecuteNonQuery();
                        }
                    } while (query.EndsWith(";"));
                }
                finally
                {
                    CloseFileStep();
                }
            }

            private void OpenFileStep()
            {
                Reader = new StreamReader(Filename);
            }

            private void CloseFileStep()
            {
                Reader?.Close();
            }

            private string ReadQueryStep()
            {
                string ret = "";
                string? line;
                while ((line = Reader.ReadLine()) != null)
                {
                    ret += line;
                    if (line.EndsWith(";"))
                    {
                        break;
                    }
                }

                return ret;
            }
        }

    }
}
