﻿
DROP TABLE IF EXISTS public.transfer;
DROP TABLE IF EXISTS public.transactionbudgetitem;
DROP TABLE IF EXISTS public.transaction;
DROP TABLE IF EXISTS public.budgetitem;
DROP TABLE IF EXISTS public.budget;
DROP TABLE IF EXISTS public.income;
DROP TABLE IF EXISTS public.productcategory;
DROP TABLE IF EXISTS public.product;
DROP TABLE IF EXISTS public.wallet;
DROP TABLE IF EXISTS public.unit;
DROP TABLE IF EXISTS public.setting;
DROP TABLE IF EXISTS public.place;
DROP TABLE IF EXISTS public.category;


CREATE TABLE public.category (
	id serial NOT NULL,
	name text NOT NULL,
	CONSTRAINT category_pk PRIMARY KEY (id),
	CONSTRAINT category_un UNIQUE (name)
);


CREATE TABLE public.place (
	id serial NOT NULL,
	name text NOT NULL,
	CONSTRAINT place_pk PRIMARY KEY (id),
	CONSTRAINT place_un UNIQUE (name)
);


CREATE TABLE public.setting (
	id serial NOT NULL,
	name text NOT NULL,
	value text NOT NULL,
	CONSTRAINT setting_pk PRIMARY KEY (id),
	CONSTRAINT setting_un UNIQUE (name)
);


CREATE TABLE public.unit (
	id serial NOT NULL,
	name text NOT NULL,
	CONSTRAINT unit_pk PRIMARY KEY (id),
	CONSTRAINT unit_un UNIQUE (name)
);


CREATE TABLE public.wallet (
	id serial NOT NULL,
	name text NOT NULL,
	CONSTRAINT wallet_pk PRIMARY KEY (id),
	CONSTRAINT wallet_un UNIQUE (name)
);


CREATE TABLE public.product (
	id serial NOT NULL,
	name text NOT NULL,
	unitid integer NOT NULL,
	CONSTRAINT product_pk PRIMARY KEY (id),
	CONSTRAINT product_un UNIQUE (name)
);


CREATE TABLE public.productcategory (
	id serial NOT NULL,
	productid integer NOT NULL,
	categoryid integer NOT NULL,
	CONSTRAINT productcategory_pk PRIMARY KEY (id),
	CONSTRAINT productcategory_fk FOREIGN KEY (productid) REFERENCES public.product(id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT productcategory_fk2 FOREIGN KEY (categoryid) REFERENCES public.category(id) ON DELETE RESTRICT ON UPDATE RESTRICT
);


CREATE TABLE public.income (
	id serial NOT NULL,
	description text NOT NULL,
	walletid integer NOT NULL,
	amount numeric NOT NULL,
	entrydate date NOT NULL,
	CONSTRAINT income_pk PRIMARY KEY (id),
	CONSTRAINT income_fk FOREIGN KEY (walletid) REFERENCES public.wallet(id) ON DELETE RESTRICT ON UPDATE RESTRICT
);


CREATE TABLE public.budget (
	id serial NOT NULL,
	startdate date NOT NULL,
	enddate date NOT NULL,
	CONSTRAINT budget_pk PRIMARY KEY (id)
);


CREATE TABLE public.budgetitem (
	id serial NOT NULL,
	budgetid integer NOT NULL,
	description text NOT NULL,
	amount numeric NOT NULL,
	CONSTRAINT budgetitem_pk PRIMARY KEY (id),
	CONSTRAINT budgetitem_fk FOREIGN KEY (budgetid) REFERENCES public.budget(id) ON DELETE RESTRICT ON UPDATE RESTRICT
);


CREATE TABLE public.transaction (
	id serial NOT NULL,
	walletid integer NOT NULL,
	placeid integer NOT NULL,
	productid integer NOT NULL,
	quantity numeric NOT NULL,
	amount numeric NOT NULL,
	entrydate date NOT NULL,
	isinbudget boolean NOT NULL DEFAULT false,
	CONSTRAINT transaction_pk PRIMARY KEY (id),
	CONSTRAINT transaction_fk FOREIGN KEY (walletid) REFERENCES public.wallet(id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT transaction_fk2 FOREIGN KEY (placeid) REFERENCES public.category(id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT transaction_fk3 FOREIGN KEY (productid) REFERENCES public.product(id) ON DELETE RESTRICT ON UPDATE RESTRICT
);


CREATE TABLE public.transactionbudgetitem (
	id serial NOT NULL,
	transactionid integer NOT NULL,
	budgetitemid integer NOT NULL,
	CONSTRAINT transactionbudgetitem_pk PRIMARY KEY (id),
	CONSTRAINT transactionbudgetitem_fk FOREIGN KEY (transactionid) REFERENCES public.transaction(id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT transactionbudgetitem_fk2 FOREIGN KEY (budgetitemid) REFERENCES public.budgetitem(id) ON DELETE RESTRICT ON UPDATE RESTRICT
);


CREATE TABLE public.transfer (
	id serial NOT NULL,
	sourcewalletid integer NOT NULL,
	targetwalletid integer NOT NULL,
	amount numeric NOT NULL,
	entrydate date NOT NULL,
	CONSTRAINT transfer_pk PRIMARY KEY (id),
	CONSTRAINT transfer_fk FOREIGN KEY (sourcewalletid) REFERENCES public.wallet(id) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT transfer_fk2 FOREIGN KEY (targetwalletid) REFERENCES public.category(id) ON DELETE RESTRICT ON UPDATE RESTRICT
);
