﻿using FamilyFinances.Control;

namespace FamilyFinances
{
    public partial class MainWindow : Form
    {
        private const string MainWindowSize = "MainWindowSize";
        private const string MainWindowLocation = "MainWindowLocation";

        public MainWindow()
        {
            InitializeComponent();
            if (Helper.IsDesignMode)
            {
                return;
            }

            //tabControl1.SelectedIndexChanged += TabControl1_SelectedIndexChanged;
            //tabControl1.SelectedTab = invoicesPage;
            TabControl1_SelectedIndexChanged(this, EventArgs.Empty);

            LoadSettings();
            ResizeEnd += MainWindow_ResizeEnd;
        }

        private void LoadSettings()
        {
            Size = Settings.Instance.GetSize(MainWindowSize, Size);
            Location = Settings.Instance.GetPoint(MainWindowLocation, Location);
        }

        private void TabControl1_SelectedIndexChanged(object? sender, EventArgs e)
        {
            foreach (var control in tabControl1.SelectedTab.Controls)
            {
                if (control is IReloadable reloadable)
                {
                    reloadable.Reload();
                }
            }
        }

        private void MainWindow_ResizeEnd(object? sender, EventArgs e)
        {
            Settings.Instance.Set(MainWindowSize, Size);
            Settings.Instance.Set(MainWindowLocation, Location);
        }

    }
}
