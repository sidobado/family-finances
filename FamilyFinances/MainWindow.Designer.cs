﻿namespace FamilyFinances
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            tabControl1 = new TabControl();
            transactionsPage = new TabPage();
            transactionsControl1 = new Control.InvoiceListControl();
            incomePage = new TabPage();
            incomeControl1 = new Control.IncomeControl();
            salaryPage = new TabPage();
            salaryControl1 = new Control.SalaryControl();
            walletsPage = new TabPage();
            walletsControl1 = new Control.WalletsControl();
            planningPage = new TabPage();
            plannerControl2 = new Control.PlannerControl();
            transfersPage = new TabPage();
            transfersControl1 = new Control.TransfersControl();
            raportsPage = new TabPage();
            raportsControl1 = new Control.RaportsControl();
            productsPage = new TabPage();
            productsControl1 = new Control.ProductsControl();
            categoriesPage = new TabPage();
            categoriesControl1 = new Control.CategoriesControl();
            placesPage = new TabPage();
            placeControl1 = new Control.PlaceControl();
            unitsPage = new TabPage();
            unitsControl1 = new Control.UnitsControl();
            tabControl1.SuspendLayout();
            transactionsPage.SuspendLayout();
            incomePage.SuspendLayout();
            salaryPage.SuspendLayout();
            walletsPage.SuspendLayout();
            planningPage.SuspendLayout();
            transfersPage.SuspendLayout();
            raportsPage.SuspendLayout();
            productsPage.SuspendLayout();
            categoriesPage.SuspendLayout();
            placesPage.SuspendLayout();
            unitsPage.SuspendLayout();
            SuspendLayout();
            // 
            // tabControl1
            // 
            tabControl1.Controls.Add(transactionsPage);
            tabControl1.Controls.Add(incomePage);
            tabControl1.Controls.Add(salaryPage);
            tabControl1.Controls.Add(walletsPage);
            tabControl1.Controls.Add(planningPage);
            tabControl1.Controls.Add(transfersPage);
            tabControl1.Controls.Add(raportsPage);
            tabControl1.Controls.Add(productsPage);
            tabControl1.Controls.Add(categoriesPage);
            tabControl1.Controls.Add(placesPage);
            tabControl1.Controls.Add(unitsPage);
            tabControl1.Dock = DockStyle.Fill;
            tabControl1.Location = new Point(0, 0);
            tabControl1.Name = "tabControl1";
            tabControl1.SelectedIndex = 0;
            tabControl1.Size = new Size(800, 450);
            tabControl1.TabIndex = 3;
            // 
            // transactionsPage
            // 
            transactionsPage.Controls.Add(transactionsControl1);
            transactionsPage.Location = new Point(4, 24);
            transactionsPage.Name = "transactionsPage";
            transactionsPage.Padding = new Padding(3);
            transactionsPage.Size = new Size(792, 422);
            transactionsPage.TabIndex = 0;
            transactionsPage.Text = "Transakcje";
            transactionsPage.UseVisualStyleBackColor = true;
            // 
            // transactionsControl1
            // 
            transactionsControl1.Dock = DockStyle.Fill;
            transactionsControl1.Location = new Point(3, 3);
            transactionsControl1.Name = "transactionsControl1";
            transactionsControl1.Size = new Size(786, 416);
            transactionsControl1.TabIndex = 0;
            // 
            // incomePage
            // 
            incomePage.Controls.Add(incomeControl1);
            incomePage.Location = new Point(4, 24);
            incomePage.Name = "incomePage";
            incomePage.Padding = new Padding(3);
            incomePage.Size = new Size(792, 422);
            incomePage.TabIndex = 9;
            incomePage.Text = "Wpływy";
            incomePage.UseVisualStyleBackColor = true;
            // 
            // incomeControl1
            // 
            incomeControl1.Dock = DockStyle.Fill;
            incomeControl1.Location = new Point(3, 3);
            incomeControl1.Name = "incomeControl1";
            incomeControl1.Size = new Size(786, 416);
            incomeControl1.TabIndex = 0;
            // 
            // salaryPage
            // 
            salaryPage.Controls.Add(salaryControl1);
            salaryPage.Location = new Point(4, 24);
            salaryPage.Name = "salaryPage";
            salaryPage.Padding = new Padding(3);
            salaryPage.Size = new Size(792, 422);
            salaryPage.TabIndex = 10;
            salaryPage.Text = "Pensje";
            salaryPage.UseVisualStyleBackColor = true;
            // 
            // salaryControl1
            // 
            salaryControl1.Dock = DockStyle.Fill;
            salaryControl1.Location = new Point(3, 3);
            salaryControl1.Name = "salaryControl1";
            salaryControl1.Size = new Size(786, 416);
            salaryControl1.TabIndex = 0;
            // 
            // walletsPage
            // 
            walletsPage.Controls.Add(walletsControl1);
            walletsPage.Location = new Point(4, 24);
            walletsPage.Name = "walletsPage";
            walletsPage.Padding = new Padding(3);
            walletsPage.Size = new Size(792, 422);
            walletsPage.TabIndex = 1;
            walletsPage.Text = "Portfele";
            walletsPage.UseVisualStyleBackColor = true;
            // 
            // walletsControl1
            // 
            walletsControl1.Dock = DockStyle.Fill;
            walletsControl1.Location = new Point(3, 3);
            walletsControl1.Name = "walletsControl1";
            walletsControl1.Size = new Size(786, 416);
            walletsControl1.TabIndex = 0;
            // 
            // planningPage
            // 
            planningPage.Controls.Add(plannerControl2);
            planningPage.Location = new Point(4, 24);
            planningPage.Name = "planningPage";
            planningPage.Padding = new Padding(3);
            planningPage.Size = new Size(792, 422);
            planningPage.TabIndex = 2;
            planningPage.Text = "Planowanie";
            planningPage.UseVisualStyleBackColor = true;
            // 
            // plannerControl2
            // 
            plannerControl2.Dock = DockStyle.Fill;
            plannerControl2.Location = new Point(3, 3);
            plannerControl2.Name = "plannerControl2";
            plannerControl2.Size = new Size(786, 416);
            plannerControl2.TabIndex = 0;
            // 
            // transfersPage
            // 
            transfersPage.Controls.Add(transfersControl1);
            transfersPage.Location = new Point(4, 24);
            transfersPage.Name = "transfersPage";
            transfersPage.Padding = new Padding(3);
            transfersPage.Size = new Size(792, 422);
            transfersPage.TabIndex = 3;
            transfersPage.Text = "Transfery";
            transfersPage.UseVisualStyleBackColor = true;
            // 
            // transfersControl1
            // 
            transfersControl1.Dock = DockStyle.Fill;
            transfersControl1.Location = new Point(3, 3);
            transfersControl1.Name = "transfersControl1";
            transfersControl1.Size = new Size(786, 416);
            transfersControl1.TabIndex = 0;
            // 
            // raportsPage
            // 
            raportsPage.Controls.Add(raportsControl1);
            raportsPage.Location = new Point(4, 24);
            raportsPage.Name = "raportsPage";
            raportsPage.Padding = new Padding(3);
            raportsPage.Size = new Size(792, 422);
            raportsPage.TabIndex = 4;
            raportsPage.Text = "Raporty";
            raportsPage.UseVisualStyleBackColor = true;
            // 
            // raportsControl1
            // 
            raportsControl1.Dock = DockStyle.Fill;
            raportsControl1.Location = new Point(3, 3);
            raportsControl1.Name = "raportsControl1";
            raportsControl1.Size = new Size(786, 416);
            raportsControl1.TabIndex = 0;
            // 
            // productsPage
            // 
            productsPage.Controls.Add(productsControl1);
            productsPage.Location = new Point(4, 24);
            productsPage.Name = "productsPage";
            productsPage.Padding = new Padding(3);
            productsPage.Size = new Size(792, 422);
            productsPage.TabIndex = 5;
            productsPage.Text = "Produkty";
            productsPage.UseVisualStyleBackColor = true;
            // 
            // productsControl1
            // 
            productsControl1.Dock = DockStyle.Fill;
            productsControl1.Location = new Point(3, 3);
            productsControl1.Name = "productsControl1";
            productsControl1.Size = new Size(786, 416);
            productsControl1.TabIndex = 0;
            // 
            // categoriesPage
            // 
            categoriesPage.Controls.Add(categoriesControl1);
            categoriesPage.Location = new Point(4, 24);
            categoriesPage.Name = "categoriesPage";
            categoriesPage.Size = new Size(792, 422);
            categoriesPage.TabIndex = 6;
            categoriesPage.Text = "Kategorie";
            categoriesPage.UseVisualStyleBackColor = true;
            // 
            // categoriesControl1
            // 
            categoriesControl1.Dock = DockStyle.Fill;
            categoriesControl1.Location = new Point(0, 0);
            categoriesControl1.Name = "categoriesControl1";
            categoriesControl1.Size = new Size(792, 422);
            categoriesControl1.TabIndex = 0;
            // 
            // placesPage
            // 
            placesPage.Controls.Add(placeControl1);
            placesPage.Location = new Point(4, 24);
            placesPage.Name = "placesPage";
            placesPage.Size = new Size(792, 422);
            placesPage.TabIndex = 7;
            placesPage.Text = "Miejsca";
            placesPage.UseVisualStyleBackColor = true;
            // 
            // placeControl1
            // 
            placeControl1.Dock = DockStyle.Fill;
            placeControl1.Location = new Point(0, 0);
            placeControl1.Name = "placeControl1";
            placeControl1.Size = new Size(792, 422);
            placeControl1.TabIndex = 0;
            // 
            // unitsPage
            // 
            unitsPage.Controls.Add(unitsControl1);
            unitsPage.Location = new Point(4, 24);
            unitsPage.Name = "unitsPage";
            unitsPage.Size = new Size(792, 422);
            unitsPage.TabIndex = 8;
            unitsPage.Text = "Jednostki";
            unitsPage.UseVisualStyleBackColor = true;
            // 
            // unitsControl1
            // 
            unitsControl1.Dock = DockStyle.Fill;
            unitsControl1.Location = new Point(0, 0);
            unitsControl1.Name = "unitsControl1";
            unitsControl1.Size = new Size(792, 422);
            unitsControl1.TabIndex = 0;
            // 
            // MainWindow
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(tabControl1);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "MainWindow";
            StartPosition = FormStartPosition.Manual;
            Text = "Finanse Wołczaków :)";
            tabControl1.ResumeLayout(false);
            transactionsPage.ResumeLayout(false);
            incomePage.ResumeLayout(false);
            salaryPage.ResumeLayout(false);
            walletsPage.ResumeLayout(false);
            planningPage.ResumeLayout(false);
            transfersPage.ResumeLayout(false);
            raportsPage.ResumeLayout(false);
            productsPage.ResumeLayout(false);
            categoriesPage.ResumeLayout(false);
            placesPage.ResumeLayout(false);
            unitsPage.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private TabControl tabControl1;
        private TabPage transactionsPage;
        private TabPage walletsPage;
        private TabPage planningPage;
        private TabPage transfersPage;
        private TabPage raportsPage;
        private TabPage productsPage;
        private TabPage categoriesPage;
        private TabPage placesPage;
        private TabPage unitsPage;
        private Control.CategoriesControl categoriesControl1;
        private Control.WalletsControl walletsControl1;
        private Control.UnitsControl unitsControl1;
        private Control.PlaceControl placeControl1;
        private Control.ProductsControl productsControl1;
        private Control.InvoiceListControl transactionsControl1;
        private Control.TransfersControl transfersControl1;
        private TabPage incomePage;
        private Control.IncomeControl incomeControl1;
        private Control.RaportsControl raportsControl1;
        private Control.PlannerControl plannerControl2;
        private TabPage salaryPage;
        private Control.SalaryControl salaryControl1;
    }
}