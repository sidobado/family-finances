﻿namespace FamilyFinances.DTO
{
    public class ProductWithTransactionDTO
    {
        public Entity.Product? Product { get; set; }
        public Entity.Transaction? Transaction { get; set; }
    }
}
