﻿using FamilyFinances.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyFinances.DTO
{
    public class InvoiceDTO
    {
        public DateTime Date { get; private set; }
        public Place Place { get; private set; }
        public Wallet Wallet { get; private set; }
        public decimal Amount { get; private set; }
        public string BudgetName { get; private set; }
        public int[] TransactionIds { get; private set; }

        public InvoiceDTO(DateTime date, Place place, Wallet wallet, decimal amount, string budgetName, int[] transactionIds)
        {
            Date = date;
            Place = place;
            Wallet = wallet;
            Amount = amount;
            BudgetName = budgetName;
            TransactionIds = transactionIds;
        }
    }
}
