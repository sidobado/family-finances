﻿using FamilyFinances.Entity;

namespace FamilyFinances.DTO
{
    public class WalletBalanceDTO
    {
        public Wallet Wallet { get; set; } = new();
        public decimal Balance { get; set; } = 0.0m;
    }
}
