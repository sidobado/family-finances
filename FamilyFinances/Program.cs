using System.Diagnostics;
using ZefirLib.ORM;

namespace FamilyFinances
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            // To customize application configuration such as set high DPI settings or default font,
            // see https://aka.ms/applicationconfiguration.
            ApplicationConfiguration.Initialize();

            bool reinstall = (args.Contains("-reinstall") || args.Contains("-reinstallwithfixtures"));
            bool loadFixtures = args.Contains("-reinstallwithfixtures");
            bool useDevelopment = args.Contains("-dev");

            Helper.SetUseDevelopment(useDevelopment);
            Database.SetConnectionString(Helper.GetDatabaseConnectionString());

            var migrations = new Sql.Migration();
            if (reinstall)
            {
                migrations.Reinstall();
            }
            else
            {
                migrations.Migrate();
            }

            if (loadFixtures)
            {
                var fixtures = new Fixtures();
                fixtures.Run();
            }

            Application.Run(new MainWindow());
        }
    }
}