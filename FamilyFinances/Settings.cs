﻿using FamilyFinances.Entity;
using System.Windows;
using ZefirLib.ORM;

namespace FamilyFinances
{
    public class Settings
    {
        private enum SettingEnum
        {
            DatabaseVersion,
            LastUsedWalletId,
            LastUsedPlaceId,
            LastUsedDate,
        }

        private readonly Dictionary<string, Setting> Dictionary = new();
        private static Settings? _instance = null;
        public static Settings Instance
        {
            get
            {
                _instance ??= new Settings();
                return _instance;
            }
        }
        
        public Settings()
        {
            using var repo = new Repository<Setting>();
            var data = repo.GetAll();

            foreach (var item in data)
            {
                Dictionary.Add(item.Name, item);
            }
        }

        private void EnsureSettingExist(SettingEnum setting, object defaultValue)
        {
            EnsureSettingExist(setting.ToString(), defaultValue);
        }

        private void EnsureSettingExist(string setting, object defaultValue)
        {
            if (Dictionary.ContainsKey(setting)) return;
            using var repo = new Repository<Setting>();
            var s = new Setting
            {
                Name = setting,
                Value = defaultValue?.ToString() ?? ""
            };
            repo.Save(s);
            Dictionary.Add(setting, s);
        }

        public void Set(string name, Size value)
        {
            string strValue = value.Width.ToString() + "x" + value.Height.ToString();
            EnsureSettingExist(name, strValue);
            Dictionary[name].Value = strValue;
            using var repo = new Repository<Setting>();
            repo.Save(Dictionary[name]);
        }

        public void Set(string name, Point value)
        {
            string strValue = value.X.ToString() + "x" + value.Y.ToString();
            EnsureSettingExist(name, strValue);
            Dictionary[name].Value = strValue;
            using var repo = new Repository<Setting>();
            repo.Save(Dictionary[name]);
        }

        public Size GetSize(string name, Size defaultValue)
        {
            EnsureSettingExist(name, defaultValue.Width.ToString() + "x" + defaultValue.Height.ToString());
            string strValue = Dictionary[name].Value;
            var values = strValue.Split('x');

            return new Size(int.Parse(values[0]), int.Parse(values[1]));
        }

        public Point GetPoint(string name, Point defaultValue)
        {
            EnsureSettingExist(name, defaultValue.X.ToString() + "x" + defaultValue.Y.ToString());
            string strValue = Dictionary[name].Value;
            var values = strValue.Split('x');

            return new Point(int.Parse(values[0]), int.Parse(values[1]));
        }

        public int LastUsedWalletId
        {
            get
            {
                SettingEnum e = SettingEnum.LastUsedWalletId;
                EnsureSettingExist(e, 0);
                return int.Parse(Dictionary[e.ToString()].Value);
            }
            set
            {
                using var repo = new Repository<Setting>();
                SettingEnum e = SettingEnum.LastUsedWalletId;
                Dictionary[e.ToString()].Value = value.ToString();
                repo.Save(Dictionary[e.ToString()]);
            }
        }

        public int LastUsedPlaceId
        {
            get
            {
                SettingEnum e = SettingEnum.LastUsedPlaceId;
                EnsureSettingExist(e, 0);
                return int.Parse(Dictionary[e.ToString()].Value);
            }
            set
            {
                using var repo = new Repository<Setting>();
                SettingEnum e = SettingEnum.LastUsedPlaceId;
                Dictionary[e.ToString()].Value = value.ToString();
                repo.Save(Dictionary[e.ToString()]);
            }
        }

        public DateTime LastUsedDate
        {
            get
            {
                SettingEnum e = SettingEnum.LastUsedDate;
                EnsureSettingExist(e, DateTime.Now);
                return DateTime.Parse(Dictionary[e.ToString()].Value);
            }
            set
            {
                using var repo = new Repository<Setting>();
                SettingEnum e = SettingEnum.LastUsedDate;
                Dictionary[e.ToString()].Value = value.ToString();
                repo.Save(Dictionary[e.ToString()]);
            }
        }

        public int DatabaseVersion
        {
            get
            {
                SettingEnum e = SettingEnum.DatabaseVersion;
                EnsureSettingExist(e, 1);
                return int.Parse(Dictionary[e.ToString()].Value);
            }
            set
            {
                using var repo = new Repository<Setting>();
                SettingEnum e = SettingEnum.DatabaseVersion;
                Dictionary[e.ToString()].Value = value.ToString();
                repo.Save(Dictionary[e.ToString()]);
            }
        }
    }
}
