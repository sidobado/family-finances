﻿namespace FamilyFinances.Control
{
    partial class InvoiceListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DataGridViewCellStyle dataGridViewCellStyle4 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle5 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle6 = new DataGridViewCellStyle();
            button1 = new Button();
            panel1 = new Panel();
            dateTo = new DateTimePicker();
            dateFrom = new DateTimePicker();
            addInvoiceButton = new Button();
            button2 = new Button();
            button4 = new Button();
            panel2 = new Panel();
            gridView = new DataGridView();
            DateColumn = new DataGridViewTextBoxColumn();
            PlaceColumn = new DataGridViewTextBoxColumn();
            WalletColumn = new DataGridViewTextBoxColumn();
            AmountColumn = new DataGridViewTextBoxColumn();
            InBudgetColumn = new DataGridViewTextBoxColumn();
            label1 = new Label();
            label2 = new Label();
            panel1.SuspendLayout();
            panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)gridView).BeginInit();
            SuspendLayout();
            // 
            // button1
            // 
            button1.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            button1.Location = new Point(564, 3);
            button1.Name = "button1";
            button1.Size = new Size(99, 23);
            button1.TabIndex = 0;
            button1.Text = "Ostatni miesiąc";
            button1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            panel1.Controls.Add(label2);
            panel1.Controls.Add(label1);
            panel1.Controls.Add(dateTo);
            panel1.Controls.Add(dateFrom);
            panel1.Controls.Add(addInvoiceButton);
            panel1.Controls.Add(button2);
            panel1.Controls.Add(button4);
            panel1.Controls.Add(button1);
            panel1.Dock = DockStyle.Top;
            panel1.Location = new Point(0, 0);
            panel1.Name = "panel1";
            panel1.Size = new Size(666, 30);
            panel1.TabIndex = 1;
            // 
            // dateTo
            // 
            dateTo.Format = DateTimePickerFormat.Custom;
            dateTo.Location = new Point(154, 3);
            dateTo.Name = "dateTo";
            dateTo.Size = new Size(80, 23);
            dateTo.TabIndex = 1;
            dateTo.Value = new DateTime(2023, 5, 14, 0, 0, 0, 0);
            // 
            // dateFrom
            // 
            dateFrom.Format = DateTimePickerFormat.Custom;
            dateFrom.Location = new Point(50, 3);
            dateFrom.Name = "dateFrom";
            dateFrom.Size = new Size(80, 23);
            dateFrom.TabIndex = 1;
            dateFrom.Value = new DateTime(2023, 5, 1, 0, 0, 0, 0);
            // 
            // addInvoiceButton
            // 
            addInvoiceButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            addInvoiceButton.Location = new Point(264, 3);
            addInvoiceButton.Name = "addInvoiceButton";
            addInvoiceButton.Size = new Size(76, 23);
            addInvoiceButton.TabIndex = 0;
            addInvoiceButton.Text = "+";
            addInvoiceButton.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            button2.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            button2.Location = new Point(346, 3);
            button2.Name = "button2";
            button2.Size = new Size(98, 23);
            button2.TabIndex = 0;
            button2.Text = "Ostatni tydzień";
            button2.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            button4.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            button4.Location = new Point(450, 3);
            button4.Name = "button4";
            button4.Size = new Size(108, 23);
            button4.TabIndex = 0;
            button4.Text = "Aktualny miesiąc";
            button4.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            panel2.Controls.Add(gridView);
            panel2.Dock = DockStyle.Fill;
            panel2.Location = new Point(0, 30);
            panel2.Name = "panel2";
            panel2.Size = new Size(666, 317);
            panel2.TabIndex = 2;
            // 
            // gridView
            // 
            gridView.AllowUserToAddRows = false;
            gridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = Color.FromArgb(234, 234, 234);
            gridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            gridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridViewCellStyle5.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = SystemColors.Control;
            dataGridViewCellStyle5.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle5.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = DataGridViewTriState.False;
            gridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            gridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            gridView.Columns.AddRange(new DataGridViewColumn[] { DateColumn, PlaceColumn, WalletColumn, AmountColumn, InBudgetColumn });
            gridView.Dock = DockStyle.Fill;
            gridView.Location = new Point(0, 0);
            gridView.MultiSelect = false;
            gridView.Name = "gridView";
            gridView.ReadOnly = true;
            gridView.RowHeadersVisible = false;
            gridView.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            gridView.RowTemplate.Height = 25;
            gridView.RowTemplate.Resizable = DataGridViewTriState.False;
            gridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            gridView.ShowCellToolTips = false;
            gridView.Size = new Size(666, 317);
            gridView.TabIndex = 0;
            // 
            // DateColumn
            // 
            DateColumn.HeaderText = "Data";
            DateColumn.Name = "DateColumn";
            DateColumn.ReadOnly = true;
            DateColumn.Width = 56;
            // 
            // PlaceColumn
            // 
            PlaceColumn.HeaderText = "Miejsce";
            PlaceColumn.Name = "PlaceColumn";
            PlaceColumn.ReadOnly = true;
            PlaceColumn.Width = 72;
            // 
            // WalletColumn
            // 
            WalletColumn.HeaderText = "Portfel";
            WalletColumn.Name = "WalletColumn";
            WalletColumn.ReadOnly = true;
            WalletColumn.Width = 67;
            // 
            // AmountColumn
            // 
            AmountColumn.HeaderText = "Kwota";
            AmountColumn.Name = "AmountColumn";
            AmountColumn.ReadOnly = true;
            AmountColumn.Width = 65;
            // 
            // InBudgetColumn
            // 
            InBudgetColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle6.WrapMode = DataGridViewTriState.False;
            InBudgetColumn.DefaultCellStyle = dataGridViewCellStyle6;
            InBudgetColumn.HeaderText = "Nazwa w budżecie";
            InBudgetColumn.Name = "InBudgetColumn";
            InBudgetColumn.ReadOnly = true;
            InBudgetColumn.Width = 129;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(3, 7);
            label1.Name = "label1";
            label1.Size = new Size(41, 15);
            label1.TabIndex = 2;
            label1.Text = "Zakres";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(136, 7);
            label2.Name = "label2";
            label2.Size = new Size(12, 15);
            label2.TabIndex = 2;
            label2.Text = "-";
            // 
            // InvoiceListControl
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(panel2);
            Controls.Add(panel1);
            Name = "InvoiceListControl";
            Size = new Size(666, 347);
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)gridView).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private Button button1;
        private Panel panel1;
        private Button button2;
        private Button button4;
        private Panel panel2;
        private DataGridView gridView;
        private DateTimePicker dateTo;
        private DateTimePicker dateFrom;
        private DataGridViewTextBoxColumn DateColumn;
        private DataGridViewTextBoxColumn PlaceColumn;
        private DataGridViewTextBoxColumn WalletColumn;
        private DataGridViewTextBoxColumn AmountColumn;
        private DataGridViewTextBoxColumn InBudgetColumn;
        private Button addInvoiceButton;
        private Label label1;
        private Label label2;
    }
}
