﻿using FamilyFinances.Data;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace FamilyFinances.Control
{
    public partial class PopupDictionaryControl : UserControl
    {
        const int VerticalPadding = 0;
        private readonly IDictionaryConnector? Connector;
        private string LastSearchText = string.Empty;

        private PopupDictionaryControl()
        {
            InitializeComponent();
            Hide();
        }

        public PopupDictionaryControl(DataGridView dataGridView, IDictionaryConnector connector) : this()
        {
            Parent = dataGridView;
            Connector = connector;
        }

        public new void Show()
        {
            SetupLocation();
            base.Show();
        }

        private void SetupLocation()
        {
            Location = new Point(0, 0);

            if (Parent is PopupDataGridView grid)
            {
                SetupLocationInPopupDataGridView(grid.CurrentCell.RowIndex, grid.CurrentCell.ColumnIndex);
            }
        }

        private void SetupLocationInPopupDataGridView(int rowIndex, int columnIndex)
        {
            var p = new Point(0, 0);

            if (rowIndex >= 0 && columnIndex >= 0)
            {
                var obj = Parent as PopupDataGridView;
                var cellRectangle = obj.GetCellDisplayRectangle(columnIndex, rowIndex, true);
                if (cellRectangle.Top < obj.Size.Height / 2)
                {
                    p.Y += cellRectangle.Bottom;
                    p.Y += VerticalPadding;
                }
                else
                {
                    p.Y = cellRectangle.Top - Size.Height - VerticalPadding;
                }


                p.X += cellRectangle.Left;
            }

            Location = p;
        }

        public bool AcceptSelectedValue()
        {
            return Connector.AcceptIndex(dgv.SelectedRows[0].Index);
        }

        public void InsertValue()
        {
            Connector.InsertValue();
        }

        public void SelectPrevious()
        {
            int currentIndex = dgv.SelectedRows[0].Index;
            if (currentIndex > 0)
            {
                dgv.Rows[currentIndex - 1].Selected = true;
            }
        }

        public void SelectNext()
        {
            int currentIndex = dgv.SelectedRows[0].Index;
            if (currentIndex < dgv.Rows.Count - 1)
            {
                dgv.Rows[currentIndex + 1].Selected = true;
            }
        }

        public void Search(string text)
        {
            if (LastSearchText == text)
            {
                return;
            }
            LastSearchText = text;

            text = Regex.Replace(text, " \\([a-zA-z]+\\)", "");
            var data = Connector.Search(text);
            Debug.WriteLine("Searching...");
            dgv.Rows.Clear();
            foreach (var row in data)
            {
                int index = dgv.Rows.Add();
                dgv.Rows[index].Cells[0].Value = row;
            }
        }
    }
}
