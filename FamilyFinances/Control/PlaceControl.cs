﻿using FamilyFinances.Entity;
using FamilyFinances.Forms;

namespace FamilyFinances.Control
{
    public class PlaceControl : BaseDictionaryGenericControl<Place, PlaceForm>
    {
        protected override string OrderBy { get; set; } = "Name";

        protected override void SetupColumns()
        {
            AddGridColumn("Id", "Identyfikator");
            AddGridColumn("Name", "Nazwa miejsca");
            grid.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
    }
}
