﻿namespace FamilyFinances.Control
{
    partial class PlannerControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            createBudgetButton = new Button();
            panel1 = new Panel();
            budgetList = new ListBox();
            panel2 = new Panel();
            dataGrid = new DataGridView();
            panel3 = new Panel();
            deleteItemButton = new Button();
            createItemButton = new Button();
            totalAmountLabel = new Label();
            IdColumn = new DataGridViewTextBoxColumn();
            DescriptionColumn = new DataGridViewTextBoxColumn();
            AmountColumn = new DataGridViewTextBoxColumn();
            UsedAmountColumn = new DataGridViewTextBoxColumn();
            panel1.SuspendLayout();
            panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGrid).BeginInit();
            panel3.SuspendLayout();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(8, 7);
            label1.Name = "label1";
            label1.Size = new Size(86, 15);
            label1.TabIndex = 1;
            label1.Text = "Lista budżetów";
            // 
            // createBudgetButton
            // 
            createBudgetButton.Location = new Point(107, 3);
            createBudgetButton.Name = "createBudgetButton";
            createBudgetButton.Size = new Size(31, 23);
            createBudgetButton.TabIndex = 2;
            createBudgetButton.Text = "+";
            createBudgetButton.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            panel1.Controls.Add(budgetList);
            panel1.Controls.Add(createBudgetButton);
            panel1.Controls.Add(label1);
            panel1.Dock = DockStyle.Left;
            panel1.Location = new Point(0, 0);
            panel1.Name = "panel1";
            panel1.Size = new Size(145, 310);
            panel1.TabIndex = 3;
            // 
            // budgetList
            // 
            budgetList.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
            budgetList.FormattingEnabled = true;
            budgetList.ItemHeight = 15;
            budgetList.Location = new Point(3, 32);
            budgetList.Name = "budgetList";
            budgetList.Size = new Size(139, 274);
            budgetList.TabIndex = 3;
            // 
            // panel2
            // 
            panel2.Controls.Add(dataGrid);
            panel2.Controls.Add(panel3);
            panel2.Dock = DockStyle.Fill;
            panel2.Location = new Point(145, 0);
            panel2.Name = "panel2";
            panel2.Size = new Size(362, 310);
            panel2.TabIndex = 4;
            // 
            // dataGrid
            // 
            dataGrid.AllowUserToAddRows = false;
            dataGrid.AllowUserToDeleteRows = false;
            dataGrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGrid.Columns.AddRange(new DataGridViewColumn[] { IdColumn, DescriptionColumn, AmountColumn, UsedAmountColumn });
            dataGrid.Dock = DockStyle.Fill;
            dataGrid.Location = new Point(0, 31);
            dataGrid.Name = "dataGrid";
            dataGrid.ReadOnly = true;
            dataGrid.RowTemplate.Height = 25;
            dataGrid.Size = new Size(362, 279);
            dataGrid.TabIndex = 1;
            // 
            // panel3
            // 
            panel3.Controls.Add(deleteItemButton);
            panel3.Controls.Add(createItemButton);
            panel3.Controls.Add(totalAmountLabel);
            panel3.Dock = DockStyle.Top;
            panel3.Location = new Point(0, 0);
            panel3.Name = "panel3";
            panel3.Size = new Size(362, 31);
            panel3.TabIndex = 0;
            // 
            // deleteItemButton
            // 
            deleteItemButton.Enabled = false;
            deleteItemButton.Location = new Point(87, 3);
            deleteItemButton.Name = "deleteItemButton";
            deleteItemButton.Size = new Size(108, 23);
            deleteItemButton.TabIndex = 1;
            deleteItemButton.Text = "Usuń zaznaczone";
            deleteItemButton.UseVisualStyleBackColor = true;
            // 
            // createItemButton
            // 
            createItemButton.Enabled = false;
            createItemButton.Location = new Point(6, 3);
            createItemButton.Name = "createItemButton";
            createItemButton.Size = new Size(75, 23);
            createItemButton.TabIndex = 0;
            createItemButton.Text = "Utwórz ...";
            createItemButton.UseVisualStyleBackColor = true;
            // 
            // totalAmountLabel
            // 
            totalAmountLabel.AutoSize = true;
            totalAmountLabel.Location = new Point(229, 7);
            totalAmountLabel.Name = "totalAmountLabel";
            totalAmountLabel.Size = new Size(97, 15);
            totalAmountLabel.TabIndex = 1;
            totalAmountLabel.Text = "Łącznie: 99990.00";
            // 
            // IdColumn
            // 
            IdColumn.HeaderText = "ID";
            IdColumn.Name = "IdColumn";
            IdColumn.ReadOnly = true;
            // 
            // DescriptionColumn
            // 
            DescriptionColumn.HeaderText = "Opis";
            DescriptionColumn.Name = "DescriptionColumn";
            DescriptionColumn.ReadOnly = true;
            // 
            // AmountColumn
            // 
            AmountColumn.HeaderText = "Kwota";
            AmountColumn.Name = "AmountColumn";
            AmountColumn.ReadOnly = true;
            // 
            // UsedAmountColumn
            // 
            UsedAmountColumn.HeaderText = "Wydano";
            UsedAmountColumn.Name = "UsedAmountColumn";
            UsedAmountColumn.ReadOnly = true;
            // 
            // PlannerControl
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(panel2);
            Controls.Add(panel1);
            Name = "PlannerControl";
            Size = new Size(507, 310);
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dataGrid).EndInit();
            panel3.ResumeLayout(false);
            panel3.PerformLayout();
            ResumeLayout(false);
        }

        #endregion
        private Label label1;
        private Button createBudgetButton;
        private Panel panel1;
        private Panel panel2;
        private DataGridView dataGrid;
        private Panel panel3;
        private Button deleteItemButton;
        private Button createItemButton;
        private ListBox budgetList;
        private Label totalAmountLabel;
        private DataGridViewTextBoxColumn IdColumn;
        private DataGridViewTextBoxColumn DescriptionColumn;
        private DataGridViewTextBoxColumn AmountColumn;
        private DataGridViewTextBoxColumn UsedAmountColumn;
    }
}
