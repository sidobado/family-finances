﻿using FamilyFinances.Data;

namespace FamilyFinances.Control
{
    public class PopupDataGridView : System.Windows.Forms.DataGridView
    {
        private readonly Dictionary<string, IDictionaryConnector> PopupsDefinitions = new();
        private readonly Dictionary<string, PopupDictionaryControl> PopupsList = new();
        private bool InEditMode = false;
        private string CurrentCellName = string.Empty;

        public PopupDataGridView() : base()
        {
            CellBeginEdit += PopupDataGridView_CellBeginEdit;
            CellEndEdit += PopupDataGridView_CellEndEdit;
            EditingControlShowing += PopupDataGridView_EditingControlShowing;
        }

        private void PopupDataGridView_CellBeginEdit(object? sender, DataGridViewCellCancelEventArgs e)
        {
            string columnName = Columns[e.ColumnIndex].Name;
            InEditMode = PopupsDefinitions.ContainsKey(columnName);
            if (!InEditMode)
            {
                return;
            }
            CurrentCellName = columnName + "-" + e.RowIndex.ToString();

            if (PopupsList.ContainsKey(CurrentCellName))
            {
                PopupsList.Remove(CurrentCellName);
            }

            var connector = PopupsDefinitions[columnName];
            connector.AttachedColumnIndex = e.ColumnIndex;
            connector.AttachedRowIndex = e.RowIndex;
            PopupsList.Add(CurrentCellName, new PopupDictionaryControl(this, connector));

            SearchInPopup(Rows[e.RowIndex].Cells[e.ColumnIndex].Value?.ToString() ?? "");
            PopupsList[CurrentCellName].Show();
        }

        private void PopupDataGridView_CellEndEdit(object? sender, DataGridViewCellEventArgs e)
        {
            string columnName = Columns[e.ColumnIndex].Name;
            if (!PopupsDefinitions.ContainsKey(columnName))
            {
                return;
            }
            PopupsList[CurrentCellName].InsertValue();
            InEditMode = false;
            PopupsList[CurrentCellName].Hide();
        }

        private void PopupDataGridView_EditingControlShowing(object? sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (!InEditMode)
            {
                return;
            }
            if (e.Control is DataGridViewTextBoxEditingControl control)
            {
                control.KeyUp -= Control_KeyUp;
                control.KeyUp += Control_KeyUp;
            }

        }

        private void Control_KeyUp(object? sender, KeyEventArgs e)
        {
            if (sender is DataGridViewTextBoxEditingControl control && InEditMode)
            {
                SearchInPopup(control.Text);
            }
        }

        public void AddPopup(string columnName, IDictionaryConnector connector)
        {
            if (PopupsDefinitions.ContainsKey(columnName))
            {
                return;
            }
            connector.DataGridView = this;
            PopupsDefinitions.Add(columnName, connector);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (!InEditMode)
            {
                return base.ProcessCmdKey(ref msg, keyData);
            }

            switch (keyData)
            {
                case Keys.Left:
                case Keys.Right:
                    return true;

                case Keys.Tab:
                case Keys.Return:
                    if (PopupsList[CurrentCellName].AcceptSelectedValue())
                    {
                        base.ProcessCmdKey(ref msg, keyData);
                        return false;
                    }
                    return true;

                case Keys.Up:
                    PopupsList[CurrentCellName].SelectPrevious();
                    return true;

                case Keys.Down:
                    PopupsList[CurrentCellName].SelectNext();
                    return true;
            }


            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void SearchInPopup(string text)
        {
            if (!InEditMode)
            {
                return;
            }
            PopupsList[CurrentCellName].Search(text);
        }

        private string GetEditingControlText()
        {
            if (!InEditMode || EditingControl is null)
            {
                return string.Empty;
            }

            if (EditingControl is DataGridViewTextBoxEditingControl ctrl)
            {
                return ctrl.Text;
            }


            return string.Empty;
        }
    }
}
