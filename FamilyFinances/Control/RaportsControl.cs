﻿using System.Diagnostics;

namespace FamilyFinances.Control
{
    public partial class RaportsControl : UserControl, IReloadable
    {
        private readonly RaportSummary Summary = new();

        public RaportsControl()
        {
            InitializeComponent();
            Dock = DockStyle.Fill;

            if (Helper.IsDesignMode)
            {
                return;
            }

            Load += RaportsControl_Load;
            datePicker.ValueChanged += DatePicker_ValueChanged;
            previousMonthButton.Click += PreviousMonthButton_Click;
            nextMonthButton.Click += NextMonthButton_Click;
        }

        private void RaportsControl_Load(object? sender, EventArgs e)
        {
            datePicker.Value = DateTime.Now;
            Reload();
        }

        private void DatePicker_ValueChanged(object? sender, EventArgs e)
        {
            CalculateSummary();
            Reload();
        }

        private void PreviousMonthButton_Click(object? sender, EventArgs e)
        {
            datePicker.Value = datePicker.Value.AddMonths(-1);
        }

        private void NextMonthButton_Click(object? sender, EventArgs e)
        {
            datePicker.Value = datePicker.Value.AddMonths(1);
        }

        public void Reload()
        {
            ReloadSummary();
            ReloadBalance();
        }

        private void ReloadBalance()
        {
            var repo = new Repository.Raport();
            var data = repo.GetWalletsBalance(Summary.DateTo);
            decimal totalBalance = 0.0m;

            walletBalanceGrid.Rows.Clear();
            foreach (var balance in data)
            {
                int rowIndex = walletBalanceGrid.Rows.Add();
                walletBalanceGrid.Rows[rowIndex].Cells["WalletColumn"].Value = balance.Wallet.Name;
                walletBalanceGrid.Rows[rowIndex].Cells["BalanceColumn"].Value = balance.Balance.ToString("N2");
                walletBalanceGrid.Rows[rowIndex].Cells["BalanceColumn"].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                totalBalance += balance.Balance;
            }

            balanceBox.Text = "Bilans całkowity: " + totalBalance.ToString("N2");
            Summary.TotalBalance = totalBalance;

        }

        private void ReloadSummary()
        {
            daysInMonth.Text = Summary.DaysInMonth.ToString();
            dayOfMonth.Text = Summary.DayOfMonth.ToString();
            startingBalance.Text = Summary.StartingBalance.ToString("N2");
            unplannedExpenses.Text = Summary.UnplannedExpenses.ToString("N2");
            unplannedExpensesPerDay.Text = Summary.UnplannedExpensesPerDay.ToString("N2");
            estimatedUnplannedExpenses.Text = Summary.EstimatedUnplannedExpenses.ToString("N2");
            plannedExpenses.Text = Summary.PlannedExpenses.ToString("N2");
            plannedFutureExpenses.Text = Summary.PlannedFutureExpenses.ToString("N2");
            incomes.Text = Summary.Incomes.ToString("N2");
            futureIncomes.Text = Summary.FutureIncomes.ToString("N2");
            difference.Text = (Summary.Difference > 0 ? "+" : "-") + Summary.Difference.ToString("N2");
            estimatedBalance.Text = Summary.EstimatedBalance.ToString("N2");
        }

        private void CalculateSummary()
        {
            Summary.DateFrom = new DateTime(datePicker.Value.Year, datePicker.Value.Month, 1);
            Summary.DayOfMonth = datePicker.Value.Day;
            Summary.DateTo = datePicker.Value;
            Summary.DaysInMonth = DateTime.DaysInMonth(Summary.DateFrom.Year, Summary.DateFrom.Month);

            var budgetRepo = new Repository.Budget();
            var budget = budgetRepo.GetByDate(Summary.DateFrom);
            var raportRepo = new Repository.Raport();
            Summary.StartingBalance = raportRepo.GetTotalBalance(Summary.DateFrom.AddSeconds(-1));
            Summary.UnplannedExpenses = raportRepo.GetUnplannedExpenses(Summary.DateFrom, Summary.DateTo);
            Summary.UnplannedExpensesPerDay = Summary.UnplannedExpenses / Summary.DayOfMonth;
            Summary.EstimatedUnplannedExpenses = Summary.UnplannedExpenses / Summary.DayOfMonth * Summary.DaysInMonth;
            Summary.PlannedExpenses = raportRepo.GetPlannedExpenses(Summary.DateFrom, Summary.DateTo);
            if (budget != null)
            {
                Summary.PlannedFutureExpenses = raportRepo.GetPlannedFutureExpenses(budget, Summary.DateFrom, Summary.DateTo);
            }
            else
            {
                Summary.PlannedFutureExpenses = 0.0m;
            }

            Summary.Incomes = raportRepo.GetIncomes(Summary.DateFrom, Summary.DateTo);
            Summary.FutureIncomes = raportRepo.GetFutureIncomes(Summary.DateFrom, Summary.DateTo);

            Summary.Difference =
                  Summary.Incomes
                + Summary.FutureIncomes
                - Summary.EstimatedUnplannedExpenses
                - Summary.PlannedExpenses
                - Summary.PlannedFutureExpenses;
            Summary.EstimatedBalance = Summary.StartingBalance + Summary.Difference;
        }

        private class RaportSummary
        {
            public DateTime DateFrom { get; set; }
            public DateTime DateTo { get; set; }
            public int DaysInMonth { get; set; }
            public int DayOfMonth { get; set; }
            public decimal TotalBalance { get; set; }
            public decimal StartingBalance { get; set; }
            public decimal UnplannedExpenses { get; set; }
            public decimal UnplannedExpensesPerDay { get; set; }
            public decimal EstimatedUnplannedExpenses { get; set; }
            public decimal PlannedExpenses { get; set; }
            public decimal PlannedFutureExpenses { get; set; }
            public decimal Incomes { get; set; }
            public decimal FutureIncomes { get; set; }
            public decimal Difference { get; set; }
            public decimal EstimatedBalance { get; set; }
        }
    }
}
