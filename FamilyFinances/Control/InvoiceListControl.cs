﻿using FamilyFinances.DTO;
using FamilyFinances.Forms;
using FamilyFinances.Repository;

namespace FamilyFinances.Control
{
    public partial class InvoiceListControl : UserControl, IReloadable
    {
        public InvoiceListControl()
        {
            InitializeComponent();

            if (Helper.IsDesignMode)
            {
                return;
            }

            dateFrom.Value = DateTime.Today.AddDays(-7);
            dateTo.Value = DateTime.Today;

            dateFrom.ValueChanged += DateRange_ValueChanged;
            dateTo.ValueChanged += DateRange_ValueChanged;

            gridView.CellDoubleClick += GridView_CellDoubleClick;
            addInvoiceButton.Click += AddInvoiceButton_Click;
        }

        private void DateRange_ValueChanged(object? sender, EventArgs e)
        {
            Reload();
        }

        public void Reload()
        {
            InvoiceDTO[]? data = null;

            Thread thread = new(() =>
            {
                var repo = new Transaction();
                data = repo.GetInvoiceList(dateFrom.Value, dateTo.Value);

            });
            SuspendLayout();
            gridView.SuspendLayout();
            thread.Start();
            gridView.Rows.Clear();
            thread.Join();

            foreach (var dto in data)
            {
                int index = gridView.Rows.Add();
                gridView.Rows[index].Tag = dto;
                gridView.Rows[index].Cells["AmountColumn"].Style.Alignment = DataGridViewContentAlignment.MiddleRight;

                gridView.Rows[index].Cells["DateColumn"].Value = dto.Date.ToString("dd.MM.yyyy");
                gridView.Rows[index].Cells["PlaceColumn"].Value = dto.Place.ToString();
                gridView.Rows[index].Cells["WalletColumn"].Value = dto.Wallet.ToString();
                gridView.Rows[index].Cells["AmountColumn"].Value = dto.Amount.ToString("N2");
                gridView.Rows[index].Cells["InBudgetColumn"].Value = dto.BudgetName;
            }
            gridView.ResumeLayout();
            ResumeLayout();
        }

        private void GridView_CellDoubleClick(object? sender, DataGridViewCellEventArgs e)
        {
            var form = new InvoiceForm();
            var dto = gridView.Rows[e.RowIndex].Tag as InvoiceDTO;
            if (dto is null)
            {
                return;
            }
            form.LoadInvoice(dto);
            var result = form.ShowDialog();
            if (result == DialogResult.OK) { Reload(); }
        }

        private void AddInvoiceButton_Click(object? sender, EventArgs e)
        {
            var form = new InvoiceForm();
            var result = form.ShowDialog();
            if (result == DialogResult.OK) { Reload(); }
        }
    }
}
