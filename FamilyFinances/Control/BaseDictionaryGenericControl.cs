﻿using FamilyFinances.Forms;
using System.Diagnostics;
using ZefirLib.ORM;

namespace FamilyFinances.Control
{
    public class BaseDictionaryGenericControl<Entity, EntityForm> : BaseDictionaryControl, IReloadable
        where Entity : EntityBase, new()
        where EntityForm : Form, IEntityForm<Entity>, new()
    {
        public BaseDictionaryGenericControl()
        {
            if (Helper.IsDesignMode) return;
            deleteButton.Click += DeleteButton_Click;

            createButton.Click += CreateButton_Click;
            grid.CellDoubleClick += Grid_CellDoubleClick;
        }

        private void DeleteButton_Click(object? sender, EventArgs e)
        {
            using var repo = new Repository<Entity>();
            for (int i = 0; i < grid.SelectedRows.Count; i++)
            {
                Entity? entity = (Entity?)grid.SelectedRows[i].Tag;
                if (entity is not null) repo.Delete(entity);
            }
            Reload();
        }

        protected virtual void CreateButton_Click(object? sender, EventArgs e)
        {
            using var form = new EntityForm();
            form.StartPosition = FormStartPosition.CenterParent;
            using var repo = new Repository<Entity>();
            var result = form.ShowDialog();
            if (result == DialogResult.OK && form.Value != null)
            {
                var item = repo.Save(form.Value);
                Reload();
                for (int i = 0; i < grid.Rows.Count; i++)
                {
                    grid.Rows[i].Selected = (grid.Rows[i].Tag as Entity).Id == item.Id;
                }
            }
        }

        private void Grid_CellDoubleClick(object? sender, DataGridViewCellEventArgs e)
        {
            Entity? item = (Entity?)grid.Rows[e.RowIndex].Tag;
            if (item is null) return;

            using var form = new EntityForm
            {
                Value = item
            };
            using var repo = new Repository<Entity>();
            var result = form.ShowDialog();
            if (result == DialogResult.OK)
            {
                repo.Save(form.Value);
                Reload();
                for (int i = 0; i < grid.Rows.Count; i++)
                {
                    grid.Rows[i].Selected = (grid.Rows[i].Tag as Entity).Id == item.Id;
                }
            }
        }

        protected override void FillGrid()
        {
            using var repo = new Repository<Entity>();
            var data = repo.GetAll(OrderBy, Limit);
            foreach (var item in data)
            {
                int row = grid.Rows.Add();
                grid.Rows[row].Tag = item;
                if (item is null) continue;

                System.Reflection.PropertyInfo[] array = item.GetType().GetProperties();
                for (int i = 0; i < array.Length; i++)
                {
                    System.Reflection.PropertyInfo? prop = array[i];
                    string val = prop.GetValue(item, null)?.ToString() ?? "";
                    object? rawVal = prop.GetValue(item, null);
                    if (rawVal == null) { continue; }

                    if (rawVal is decimal @decimal)
                    {
                        val = @decimal.ToString("N2");
                        grid.Rows[row].Cells[prop.Name].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                    }

                    if (rawVal is DateTime @datetime)
                    {
                        val = @datetime.ToString("dd.MM.yyyy");
                    }

                    if (false == grid.Columns.Contains(prop.Name))
                    {
                        AddGridColumn(prop.Name, prop.Name);
                    }

                    grid.Rows[row].Cells[prop.Name].Value = val;
                    if (rawVal != null)
                    {
                        UpdateRelatedTableValue(item, row, prop);
                    }

                    if (rawVal is EntityBase[] v)
                    {
                        var list = new List<EntityBase>(v);
                        list.Sort(delegate (EntityBase a, EntityBase b)
                        {
                            return string.Compare(a.ToString(), b.ToString());
                        });
                        string strVal = string.Join(", ", list);
                        grid.Rows[row].Cells[prop.Name].Value = strVal;
                    }

                }
            }
        }

        private void UpdateRelatedTableValue(Entity item, int row, System.Reflection.PropertyInfo prop)
        {
            foreach (var attr in prop.GetCustomAttributes(true))
            {
                if (attr.GetType() == typeof(OneToMany))
                {
                    var att = (OneToMany)attr;
                    Type myGeneric = typeof(Repository<>);
                    Type constructedClass = myGeneric.MakeGenericType(att.Entity);
                    dynamic? instance = Activator.CreateInstance(constructedClass);
                    var id = (int?)prop.GetValue(item, null);
                    if (id != null)
                    {
                        var obj = instance?.Get((int)id);
                        grid.Rows[row].Cells[prop.Name].Value = obj;
                    }
                    else
                    {
                        grid.Rows[row].Cells[prop.Name].Value = null;
                    }
                }
            }
        }

        protected override void SetupColumns()
        {
            var obj = new Entity();
            foreach (var prop in obj.GetType().GetProperties())
            {
                if (grid.Columns.Contains(prop.Name)) continue;

                grid.Columns.Add(new DataGridViewTextBoxColumn()
                {
                    Name = prop.Name,
                    HeaderText = prop.Name
                });
                Debug.WriteLine(prop.Name);
            }
        }
    }
}
