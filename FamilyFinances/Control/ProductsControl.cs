﻿using FamilyFinances.Entity;
using FamilyFinances.Forms;

namespace FamilyFinances.Control
{
    public class ProductsControl : BaseDictionaryGenericControl<Product, ProductForm>
    {
        protected override string OrderBy { get; set; } = "Name";
        protected override void SetupColumns()
        {
            AddGridColumn("Id", "Identyfikator");
            AddGridColumn("Name", "Nazwa produktu");
            AddGridColumn("UnitId", "Jednostka");
            AddGridColumn("CategoryId", "Kategorie");

            grid.Columns["CategoryId"].FillWeight = 200;
        }
    }
}
