﻿namespace FamilyFinances.Control
{
    public interface IReloadable
    {
        public void Reload();
    }
}
