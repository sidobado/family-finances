﻿using FamilyFinances.Entity;
using FamilyFinances.Forms;

namespace FamilyFinances.Control
{
    public class IncomeControl : BaseDictionaryGenericControl<Income, IncomeForm>
    {
        protected override string OrderBy { get; set; } = "Description";

        protected override void SetupColumns()
        {
            AddGridColumn("Id", "Identyfikator");
            AddGridColumn("Description", "Opis");
            AddGridColumn("WalletId", "Portfel");
            AddGridColumn("SalaryId", "Pensja");
            AddGridColumn("Amount", "Kwota");
            AddGridColumn("EntryDate", "Data transakcji");
            grid.Columns["Description"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
    }
}
