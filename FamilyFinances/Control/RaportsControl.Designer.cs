﻿namespace FamilyFinances.Control
{
    partial class RaportsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            datePicker = new DateTimePicker();
            previousMonthButton = new Button();
            nextMonthButton = new Button();
            panel1 = new Panel();
            balanceBox = new GroupBox();
            walletBalanceGrid = new DataGridView();
            WalletColumn = new DataGridViewTextBoxColumn();
            BalanceColumn = new DataGridViewTextBoxColumn();
            groupBox1 = new GroupBox();
            daysInMonth = new TextBox();
            startingBalance = new TextBox();
            estimatedBalance = new TextBox();
            difference = new TextBox();
            futureIncomes = new TextBox();
            label11 = new Label();
            incomes = new TextBox();
            label10 = new Label();
            plannedFutureExpenses = new TextBox();
            label9 = new Label();
            plannedExpenses = new TextBox();
            label8 = new Label();
            estimatedUnplannedExpenses = new TextBox();
            label7 = new Label();
            label5 = new Label();
            label6 = new Label();
            unplannedExpensesPerDay = new TextBox();
            label4 = new Label();
            dayOfMonth = new TextBox();
            label3 = new Label();
            label2 = new Label();
            label1 = new Label();
            label12 = new Label();
            unplannedExpenses = new TextBox();
            panel1.SuspendLayout();
            balanceBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)walletBalanceGrid).BeginInit();
            groupBox1.SuspendLayout();
            SuspendLayout();
            // 
            // datePicker
            // 
            datePicker.CustomFormat = "";
            datePicker.Format = DateTimePickerFormat.Custom;
            datePicker.Location = new Point(39, 4);
            datePicker.Name = "datePicker";
            datePicker.Size = new Size(92, 23);
            datePicker.TabIndex = 0;
            datePicker.Value = new DateTime(2023, 5, 5, 0, 0, 0, 0);
            // 
            // previousMonthButton
            // 
            previousMonthButton.Location = new Point(5, 4);
            previousMonthButton.Name = "previousMonthButton";
            previousMonthButton.Size = new Size(28, 23);
            previousMonthButton.TabIndex = 1;
            previousMonthButton.Text = "<";
            previousMonthButton.UseVisualStyleBackColor = true;
            // 
            // nextMonthButton
            // 
            nextMonthButton.Location = new Point(137, 4);
            nextMonthButton.Name = "nextMonthButton";
            nextMonthButton.Size = new Size(28, 23);
            nextMonthButton.TabIndex = 1;
            nextMonthButton.Text = ">";
            nextMonthButton.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            panel1.Controls.Add(previousMonthButton);
            panel1.Controls.Add(nextMonthButton);
            panel1.Controls.Add(datePicker);
            panel1.Location = new Point(3, 3);
            panel1.Name = "panel1";
            panel1.Size = new Size(171, 31);
            panel1.TabIndex = 2;
            // 
            // balanceBox
            // 
            balanceBox.Controls.Add(walletBalanceGrid);
            balanceBox.Location = new Point(407, 25);
            balanceBox.MaximumSize = new Size(500, 222);
            balanceBox.MinimumSize = new Size(240, 222);
            balanceBox.Name = "balanceBox";
            balanceBox.Size = new Size(240, 222);
            balanceBox.TabIndex = 3;
            balanceBox.TabStop = false;
            balanceBox.Text = "Bilans całkowity: 0.00";
            // 
            // walletBalanceGrid
            // 
            walletBalanceGrid.AllowUserToAddRows = false;
            walletBalanceGrid.AllowUserToDeleteRows = false;
            walletBalanceGrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            walletBalanceGrid.Columns.AddRange(new DataGridViewColumn[] { WalletColumn, BalanceColumn });
            walletBalanceGrid.Dock = DockStyle.Fill;
            walletBalanceGrid.Location = new Point(3, 19);
            walletBalanceGrid.Name = "walletBalanceGrid";
            walletBalanceGrid.ReadOnly = true;
            walletBalanceGrid.RowHeadersVisible = false;
            walletBalanceGrid.RowTemplate.Height = 25;
            walletBalanceGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            walletBalanceGrid.Size = new Size(234, 200);
            walletBalanceGrid.TabIndex = 0;
            // 
            // WalletColumn
            // 
            WalletColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            WalletColumn.HeaderText = "Portfel";
            WalletColumn.Name = "WalletColumn";
            WalletColumn.ReadOnly = true;
            // 
            // BalanceColumn
            // 
            BalanceColumn.HeaderText = "Bilans";
            BalanceColumn.Name = "BalanceColumn";
            BalanceColumn.ReadOnly = true;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(daysInMonth);
            groupBox1.Controls.Add(startingBalance);
            groupBox1.Controls.Add(estimatedBalance);
            groupBox1.Controls.Add(difference);
            groupBox1.Controls.Add(futureIncomes);
            groupBox1.Controls.Add(label11);
            groupBox1.Controls.Add(incomes);
            groupBox1.Controls.Add(label10);
            groupBox1.Controls.Add(plannedFutureExpenses);
            groupBox1.Controls.Add(label9);
            groupBox1.Controls.Add(plannedExpenses);
            groupBox1.Controls.Add(label8);
            groupBox1.Controls.Add(estimatedUnplannedExpenses);
            groupBox1.Controls.Add(label7);
            groupBox1.Controls.Add(label5);
            groupBox1.Controls.Add(label6);
            groupBox1.Controls.Add(unplannedExpenses);
            groupBox1.Controls.Add(unplannedExpensesPerDay);
            groupBox1.Controls.Add(label4);
            groupBox1.Controls.Add(label12);
            groupBox1.Controls.Add(dayOfMonth);
            groupBox1.Controls.Add(label3);
            groupBox1.Controls.Add(label2);
            groupBox1.Controls.Add(label1);
            groupBox1.Location = new Point(8, 44);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(228, 373);
            groupBox1.TabIndex = 4;
            groupBox1.TabStop = false;
            groupBox1.Text = "groupBox1";
            // 
            // daysInMonth
            // 
            daysInMonth.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            daysInMonth.Location = new Point(202, 51);
            daysInMonth.Name = "daysInMonth";
            daysInMonth.Size = new Size(20, 23);
            daysInMonth.TabIndex = 1;
            daysInMonth.Text = "99";
            // 
            // startingBalance
            // 
            startingBalance.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            startingBalance.Location = new Point(165, 22);
            startingBalance.Name = "startingBalance";
            startingBalance.Size = new Size(57, 23);
            startingBalance.TabIndex = 1;
            startingBalance.Text = "99 999.99";
            startingBalance.TextAlign = HorizontalAlignment.Right;
            // 
            // estimatedBalance
            // 
            estimatedBalance.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            estimatedBalance.Location = new Point(165, 312);
            estimatedBalance.Name = "estimatedBalance";
            estimatedBalance.Size = new Size(57, 23);
            estimatedBalance.TabIndex = 1;
            estimatedBalance.Text = "99 999.99";
            estimatedBalance.TextAlign = HorizontalAlignment.Right;
            // 
            // difference
            // 
            difference.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            difference.Location = new Point(165, 283);
            difference.Name = "difference";
            difference.Size = new Size(57, 23);
            difference.TabIndex = 1;
            difference.Text = "99 999.99";
            difference.TextAlign = HorizontalAlignment.Right;
            // 
            // futureIncomes
            // 
            futureIncomes.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            futureIncomes.Location = new Point(165, 254);
            futureIncomes.Name = "futureIncomes";
            futureIncomes.Size = new Size(57, 23);
            futureIncomes.TabIndex = 1;
            futureIncomes.Text = "99 999.99";
            futureIncomes.TextAlign = HorizontalAlignment.Right;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new Point(6, 286);
            label11.Name = "label11";
            label11.Size = new Size(0, 15);
            label11.TabIndex = 0;
            // 
            // incomes
            // 
            incomes.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            incomes.Location = new Point(165, 225);
            incomes.Name = "incomes";
            incomes.Size = new Size(57, 23);
            incomes.TabIndex = 1;
            incomes.Text = "99 999.99";
            incomes.TextAlign = HorizontalAlignment.Right;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new Point(6, 286);
            label10.Name = "label10";
            label10.Size = new Size(109, 15);
            label10.TabIndex = 0;
            label10.Text = "Estymacja miesiąca";
            // 
            // plannedFutureExpenses
            // 
            plannedFutureExpenses.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            plannedFutureExpenses.Location = new Point(165, 196);
            plannedFutureExpenses.Name = "plannedFutureExpenses";
            plannedFutureExpenses.Size = new Size(57, 23);
            plannedFutureExpenses.TabIndex = 1;
            plannedFutureExpenses.Text = "99 999.99";
            plannedFutureExpenses.TextAlign = HorizontalAlignment.Right;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new Point(6, 257);
            label9.Name = "label9";
            label9.Size = new Size(126, 15);
            label9.TabIndex = 0;
            label9.Text = "Przychody oczekiwane";
            // 
            // plannedExpenses
            // 
            plannedExpenses.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            plannedExpenses.Location = new Point(165, 167);
            plannedExpenses.Name = "plannedExpenses";
            plannedExpenses.Size = new Size(57, 23);
            plannedExpenses.TabIndex = 1;
            plannedExpenses.Text = "99 999.99";
            plannedExpenses.TextAlign = HorizontalAlignment.Right;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new Point(6, 228);
            label8.Name = "label8";
            label8.Size = new Size(62, 15);
            label8.TabIndex = 0;
            label8.Text = "Przychody";
            // 
            // estimatedUnplannedExpenses
            // 
            estimatedUnplannedExpenses.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            estimatedUnplannedExpenses.Location = new Point(165, 138);
            estimatedUnplannedExpenses.Name = "estimatedUnplannedExpenses";
            estimatedUnplannedExpenses.Size = new Size(57, 23);
            estimatedUnplannedExpenses.TabIndex = 1;
            estimatedUnplannedExpenses.Text = "99 999.99";
            estimatedUnplannedExpenses.TextAlign = HorizontalAlignment.Right;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new Point(6, 199);
            label7.Name = "label7";
            label7.Size = new Size(79, 15);
            label7.TabIndex = 0;
            label7.Text = "Wydatki (pl+)";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(6, 25);
            label5.Name = "label5";
            label5.Size = new Size(104, 15);
            label5.TabIndex = 0;
            label5.Text = "Bilans początkowy";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(6, 170);
            label6.Name = "label6";
            label6.Size = new Size(71, 15);
            label6.TabIndex = 0;
            label6.Text = "Wydatki (pl)";
            // 
            // unplannedExpensesPerDay
            // 
            unplannedExpensesPerDay.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            unplannedExpensesPerDay.Location = new Point(165, 109);
            unplannedExpensesPerDay.Name = "unplannedExpensesPerDay";
            unplannedExpensesPerDay.Size = new Size(57, 23);
            unplannedExpensesPerDay.TabIndex = 1;
            unplannedExpensesPerDay.Text = "99 999.99";
            unplannedExpensesPerDay.TextAlign = HorizontalAlignment.Right;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(6, 141);
            label4.Name = "label4";
            label4.Size = new Size(101, 15);
            label4.TabIndex = 0;
            label4.Text = "Wydatki (np - est)";
            // 
            // dayOfMonth
            // 
            dayOfMonth.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            dayOfMonth.Location = new Point(171, 51);
            dayOfMonth.Name = "dayOfMonth";
            dayOfMonth.Size = new Size(20, 23);
            dayOfMonth.TabIndex = 1;
            dayOfMonth.Text = "99";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(6, 112);
            label3.Name = "label3";
            label3.Size = new Size(122, 15);
            label3.TabIndex = 0;
            label3.Text = "Wydatki (np dziennie)";
            // 
            // label2
            // 
            label2.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            label2.AutoSize = true;
            label2.Location = new Point(192, 54);
            label2.Name = "label2";
            label2.Size = new Size(12, 15);
            label2.TabIndex = 0;
            label2.Text = "/";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(6, 54);
            label1.Name = "label1";
            label1.Size = new Size(85, 15);
            label1.TabIndex = 0;
            label1.Text = "Dzień miesiąca";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new Point(6, 83);
            label12.Name = "label12";
            label12.Size = new Size(75, 15);
            label12.TabIndex = 0;
            label12.Text = "Wydatki (np)";
            // 
            // unplannedExpenses
            // 
            unplannedExpenses.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            unplannedExpenses.Location = new Point(165, 80);
            unplannedExpenses.Name = "unplannedExpenses";
            unplannedExpenses.Size = new Size(57, 23);
            unplannedExpenses.TabIndex = 1;
            unplannedExpenses.Text = "99 999.99";
            unplannedExpenses.TextAlign = HorizontalAlignment.Right;
            // 
            // RaportsControl
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(groupBox1);
            Controls.Add(balanceBox);
            Controls.Add(panel1);
            Name = "RaportsControl";
            Size = new Size(721, 432);
            panel1.ResumeLayout(false);
            balanceBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)walletBalanceGrid).EndInit();
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private DateTimePicker datePicker;
        private Button previousMonthButton;
        private Button nextMonthButton;
        private Panel panel1;
        private GroupBox balanceBox;
        private DataGridView walletBalanceGrid;
        private DataGridViewTextBoxColumn WalletColumn;
        private DataGridViewTextBoxColumn BalanceColumn;
        private GroupBox groupBox1;
        private TextBox daysInMonth;
        private TextBox dayOfMonth;
        private Label label2;
        private Label label1;
        private TextBox unplannedExpensesPerDay;
        private Label label3;
        private TextBox estimatedUnplannedExpenses;
        private Label label4;
        private TextBox startingBalance;
        private Label label5;
        private TextBox plannedExpenses;
        private Label label6;
        private TextBox plannedFutureExpenses;
        private Label label7;
        private TextBox incomes;
        private Label label8;
        private TextBox futureIncomes;
        private Label label9;
        private TextBox estimatedBalance;
        private TextBox difference;
        private Label label11;
        private Label label10;
        private TextBox unplannedExpenses;
        private Label label12;
    }
}
