﻿using FamilyFinances.Entity;
using FamilyFinances.Forms;

namespace FamilyFinances.Control
{
    public partial class SalaryControl : BaseDictionaryGenericControl<Salary, SalaryForm>
    {
        protected override string OrderBy { get; set; } = "Name";

        protected override void SetupColumns()
        {
            AddGridColumn("Id", "Identyfikator");
            AddGridColumn("Name", "Opis");
            AddGridColumn("Amount", "Kwota");
            grid.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
    }
}
