﻿namespace FamilyFinances.Control
{
    public partial class BaseDictionaryControl : UserControl, IReloadable
    {

        protected virtual string OrderBy { get; set; } = "Id";
        protected virtual uint Limit { get; set; } = 1000;

        public BaseDictionaryControl()
        {
            InitializeComponent();
            Dock = DockStyle.Fill;

            if (Helper.IsDesignMode) return;

            Load += GridControleOnLoad;
            grid.SelectionChanged += Grid_SelectionChanged;

        }

        private void Grid_SelectionChanged(object? sender, EventArgs e)
        {
            deleteButton.Enabled = grid.SelectedRows.Count > 0;
        }

        protected void GridControleOnLoad(object? sender, EventArgs e)
        {
            SetupColumns();
            Reload();
        }

        public void Reload()
        {
            int firstDislapyedRowIndex = grid.FirstDisplayedScrollingRowIndex;
            grid.Rows.Clear();
            FillGrid();

            if (firstDislapyedRowIndex != -1)
            {
                grid.FirstDisplayedScrollingRowIndex = firstDislapyedRowIndex;
            }
        }


        protected virtual void FillGrid()
        {
        }

        protected virtual void SetupColumns()
        {
        }

        protected void AddGridColumn(string name, string header)
        {
            grid.Columns.Add(new DataGridViewTextBoxColumn()
            {
                Name = name,
                HeaderText = header
            });

            grid.Columns[name].MinimumWidth = 10;
        }

        protected void AddGridBoolColumn(string name, string header)
        {
            grid.Columns.Add(new DataGridViewCheckBoxColumn()
            {
                Name = name,
                HeaderText = header
            });

            grid.Columns[name].MinimumWidth = 10;
        }

    }


}
