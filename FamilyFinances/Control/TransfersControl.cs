﻿using FamilyFinances.Entity;
using FamilyFinances.Forms;

namespace FamilyFinances.Control
{
    public class TransfersControl : BaseDictionaryGenericControl<Transfer, TransferForm>
    {
        protected override string OrderBy { get; set; } = "EntryDate desc";
        protected override void SetupColumns()
        {
            AddGridColumn("Id", "Identyfikator");
            AddGridColumn("SourceWalletId", "Skąd");
            AddGridColumn("TargetWalletId", "Dokąd");
            AddGridColumn("Amount", "Kwota");
            AddGridColumn("EntryDate", "Data transakcji");

        }
    }
}
