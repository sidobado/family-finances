﻿using FamilyFinances.Entity;
using FamilyFinances.Forms;
using ZefirLib.ORM;

namespace FamilyFinances.Control
{
    public partial class MultiSelectBase<Entity, EntityForm> : UserControl, IReloadable
        where Entity : ZefirLib.ORM.EntityBase, new()
        where EntityForm : Form, IEntityForm<Entity>, new()
    {
        protected virtual string OrderBy { get; set; } = "Id";

        //private ICollection<Entity> _valuesCollection;
        //public ICollection<Entity> ValuesCollection 
        //{
        //    get => _valuesCollection;
        //    set; 
        //}

        public MultiSelectBase()
        {
            InitializeComponent();
            Reload();
        }

        //private void MultiSelect_Load(object? sender, EventArgs e)
        //{
        //    Reload();
        //}

        public void Reload()
        {
            using var repo = new Repository<Entity>();
            Entity[] items = repo.GetAll("name");
            var button = new Button()
            {
                Text = "Utwórz ...."
            };
            button.Click += CreateButton_Click;
            listPanel.Controls.Add(button);

            foreach (Entity item in items)
            {
                CreateButton(item);
            }
        }

        private void CreateButton(Entity item)
        {
            var button = new Button()
            {
                Text = item.ToString(),
                Tag = item
            };
            button.Click += Button_Click;
            listPanel.Controls.Add(button);
        }

        private void CreateButton_Click(object? sender, EventArgs e)
        {
            using var form = new EntityForm();
            using var repo = new Repository<Entity>();
            var result = form.ShowDialog();
            if (result == DialogResult.OK)
            {
                var item = repo.Save(form.Value);
                var button = new Button()
                {
                    Text = item.ToString(),
                    Tag = item
                };
                button.Click += Button_Click;
                selectedPanel.Controls.Add(button);
            }
        }

        private void Button_Click(object? sender, EventArgs e)
        {
            if (sender is null) return;
            var button = (Button)sender;
            if (button.Parent == listPanel)
            {
                button.Parent = selectedPanel;
            }
            else
            {
                button.Parent = listPanel;
            }
        }

        public ICollection<Entity> GetSelected()
        {
            var ret = new List<Entity>();

            foreach (var control in selectedPanel.Controls)
            {
                if (control is Button button)
                {
                    if (button.Tag is Entity entity)
                    {
                        ret.Add(entity);
                    }
                }
            }

            return ret.ToArray();
        }

        public void SetSelected(ICollection<Entity> entities)
        {
            foreach (Entity entity in entities)
            {
                var found = FindEntity(entity);
                if (found != null)
                {
                    found.Parent = selectedPanel;
                }
            }
        }

        protected Button? FindEntity(Entity entity)
        {
            foreach (var control in listPanel.Controls)
            {
                if (control is Button button)
                {
                    if (button.Tag is Entity e)
                    {
                        if (e.Id == entity.Id) return button;
                    }
                }
            }

            return null;
        }
    }
}
