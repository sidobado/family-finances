﻿using FamilyFinances.Entity;
using FamilyFinances.Forms;

namespace FamilyFinances.Control
{
    public class UnitsControl : BaseDictionaryGenericControl<Unit, UnitForm>
    {
        protected override string OrderBy { get; set; } = "Name";
        protected override void SetupColumns()
        {
            AddGridColumn("Id", "Identyfikator");
            AddGridColumn("Name", "Nazwa jednostki");
            grid.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
    }
}
