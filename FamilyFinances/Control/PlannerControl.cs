﻿using FamilyFinances.Entity;
using FamilyFinances.Forms;
using ZefirLib.ORM;

namespace FamilyFinances.Control
{
    // todo Updating budget items
    // todo Deleting budget items
    public partial class PlannerControl : UserControl
    {
        private int LastSelectedBudgetId { get; set; } = 0;
        public PlannerControl()
        {
            InitializeComponent();

            if (Helper.IsDesignMode)
            {
                return;
            }

            Reload();

            createBudgetButton.Click += CreateBudgetButton_Click;
            createItemButton.Click += CreateItemButton_Click;
            budgetList.SelectedIndexChanged += BudgetList_SelectedIndexChanged;
        }

        private void CreateBudgetButton_Click(object? sender, EventArgs e)
        {
            CreateBudget();
            Reload();
        }

        private void CreateItemButton_Click(object? sender, EventArgs e)
        {
            using var form = new BudgetItemForm();
            var result = form.ShowDialog();

            if (result != DialogResult.OK)
            {
                return;
            }

            var repeat = form.Count;
            var nextBudget = (Budget)budgetList.SelectedItem;
            var budgetItem = form.Value;
            var repo = new Repository.BudgetItem();
            do
            {
                var newBudgetItem = new BudgetItem()
                {
                    Amount = budgetItem.Amount,
                    BudgetId = nextBudget.Id,
                    Description = budgetItem.Description,
                };
                repo.Save(newBudgetItem);
                repeat--;
                if (repeat > 0)
                {
                    nextBudget = GetNextBudget(nextBudget);
                }
            } while (repeat > 0);

            Reload();
        }

        private void BudgetList_SelectedIndexChanged(object? sender, EventArgs e)
        {
            ReloadBudgetItems();
            createItemButton.Enabled = budgetList.SelectedItem != null;
        }

        private void Reload()
        {
            ReloadBudgets();
            ReloadBudgetItems();
            createItemButton.Enabled = budgetList.SelectedItem != null;
        }

        private void ReloadBudgets()
        {
            budgetList.Items.Clear();
            budgetList.Items.AddRange((new Repository<Budget>()).GetAll("startdate desc"));

            if (budgetList.Items.Count == 0)
            {
                return;
            }

            for (int i = 0; i < budgetList.Items.Count; i++)
            {
                var budget = ((Budget)budgetList.Items[i]);
                if (LastSelectedBudgetId == 0 && budget.StartDate <= DateTime.Now && budget.EndDate >= DateTime.Now)
                {
                    budgetList.SelectedIndex = i;
                    break;
                }
                else if (budget.Id == LastSelectedBudgetId)
                {
                    budgetList.SelectedIndex = i;
                    break;
                }
            }
        }

        private void ReloadBudgetItems()
        {
            decimal totalAmount = 0.0m;
            if (budgetList.SelectedItem == null)
            {
                totalAmountLabel.Text = "Łącznie: 0.00";
                return;
            }
            var budget = (Budget)budgetList.SelectedItem;
            LastSelectedBudgetId = budget.Id;
            var raportsRepo = new Repository.Raport();
            var itemRepo = new Repository.BudgetItem();
            var items = itemRepo.GetItems(budget);
            dataGrid.Rows.Clear();
            foreach (var item in items)
            {
                var index = dataGrid.Rows.Add(
                    item.Id,
                    item.Description,
                    item.Amount.ToString("N2"),
                    raportsRepo.GetBudgetItemExpenses(item).ToString("N2")
                    );
                dataGrid.Rows[index].Tag = item;
                dataGrid.Rows[index].Cells["AmountColumn"].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGrid.Rows[index].Cells["UsedAmountColumn"].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                totalAmount += item.Amount;
            }

            totalAmountLabel.Text = "Łącznie: " + totalAmount.ToString("N2");
        }

        private static Budget CreateBudget()
        {
            var repo = new Repository.Budget();
            var budget = repo.GetNewest();

            int year = budget?.StartDate.Year ?? DateTime.Now.Year;
            int month = budget?.StartDate.Month ?? DateTime.Now.Month - 1;

            int nextYear = year;
            int nextMonth = month + 1;

            if (nextMonth > 12)
            {
                nextYear++;
                nextMonth = 1;
            }

            var endDate = new DateTime(nextMonth + 1 > 12 ? nextYear + 1 : nextYear, nextMonth + 1 > 12 ? 1 : nextMonth + 1, 1);
            endDate = endDate.AddSeconds(-1);

            var nextBudget = new Budget()
            {
                StartDate = new DateTime(nextYear, nextMonth, 1),
                EndDate = endDate,
            };

            repo.Save(nextBudget);

            return nextBudget;
        }

        private Budget GetNextBudget(Budget nextBudget)
        {
            var repo = new Repository.Budget();
            var budget = repo.GetNextBudget(nextBudget);

            if (budget != null)
            {
                return budget;
            }

            return CreateBudget();
        }
    }
}
