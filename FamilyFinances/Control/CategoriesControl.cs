﻿using FamilyFinances.Entity;
using FamilyFinances.Forms;

namespace FamilyFinances.Control
{
    public class CategoriesControl : BaseDictionaryGenericControl<Category, CategoryForm>
    {
        protected override string OrderBy { get; set; } = "Name";
        protected override void SetupColumns()
        {
            AddGridColumn("Id", "Identyfikator");
            AddGridColumn("Name", "Nazwa kategorii");
            grid.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
    }
}
