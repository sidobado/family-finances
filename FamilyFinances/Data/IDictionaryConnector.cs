﻿namespace FamilyFinances.Data
{
    public interface IDictionaryConnector
    {
        public int AttachedColumnIndex { get; set; }
        public int AttachedRowIndex { get; set; }
        public DataGridView DataGridView { get; set; }
        public string[] LastData { get; }

        /// <summary>
        /// Fills DGV with data selected from PopupDictionaryControl.
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>True on succesfully fill DGV, false otherwise.</returns>
        bool AcceptIndex(int rowIndex);
        public void InsertValue();
        string[] Search(string text);
    }
}
