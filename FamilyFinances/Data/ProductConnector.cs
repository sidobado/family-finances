﻿using FamilyFinances.DTO;
using FamilyFinances.Entity;
using FamilyFinances.Forms;
using ZefirLib.ORM;

namespace FamilyFinances.Data
{
    public class ProductConnector : IDictionaryConnector
    {
        private Repository.Product ProductRepo = new();
        private Repository<Unit> UnitRepo = new();

        public int AttachedColumnIndex { get; set; }
        public int AttachedRowIndex { get; set; }
        public object? ValueToInsert { get; set; }
        public int LastRowIndex { get; set; }

        public DataGridView? DataGridView { get; set; }

        public string[] LastData { get; private set; } = Array.Empty<string>();
        private ProductWithTransactionDTO[] LastSearchResult = Array.Empty<ProductWithTransactionDTO>();

        public bool AcceptIndex(int rowIndex)
        {
            LastRowIndex = rowIndex;
            if (rowIndex == LastSearchResult.Length)
            {
                using var form = new ProductForm();
                if (form.ShowDialog() == DialogResult.OK && form.Value != null)
                {
                    using var repo = new Repository<Product>();
                    var entity = repo.Save(form.Value);
                    ValueToInsert = entity;
                }
            }


            return true;
        }

        public void InsertValue()
        {
            if (LastRowIndex == LastSearchResult.Length)
            {
                DataGridView.Rows[AttachedRowIndex].Cells[AttachedColumnIndex].Value = ValueToInsert;
            }
            else
            {
                DataGridView.Rows[AttachedRowIndex].Cells[AttachedColumnIndex].Value = LastSearchResult[LastRowIndex].Product;
                if (LastSearchResult[LastRowIndex].Transaction != null)
                {
                    DataGridView.Rows[AttachedRowIndex].Cells["Quantity"].Value = LastSearchResult[LastRowIndex].Transaction.Quantity;
                    DataGridView.Rows[AttachedRowIndex].Cells["Amount"].Value = LastSearchResult[LastRowIndex].Transaction.Amount.ToString("N2");
                }
            }
        }

        public string[] Search(string text)
        {
            var data = LastSearchResult = ProductRepo.FindProductsWithLastTransaction(text);
            var result = new string[data.Length + 1];

            int it = 0;
            for (; it < data.Length; it++)
            {
                string rowText = data[it].Product.ToString();
                if (data[it].Transaction != null)
                {
                    decimal price = data[it].Transaction.Amount / data[it].Transaction.Quantity;
                    Unit? unit = UnitRepo.Get(data[it].Product.UnitId);
                    string priceText = price.ToString("N2") + "/" + unit.Name;
                    rowText += $" ({priceText})";
                }
                result[it] = rowText;
            }
            result[it] = "<< Utwórz nowy produkt >>";

            LastData = result;

            return LastData;
        }
    }
}
