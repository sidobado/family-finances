﻿using IniParser;
using IniParser.Model;
using System.Diagnostics;
using System.Reflection;
using ZefirLib.ORM;

namespace FamilyFinances
{
    public static class Helper
    {
        private static string SettingsCategory = "Development";

        public static int GetApplicationVersion()
        {
            return 4;
        }

        public static object? FindAttributeInProperty(PropertyInfo property, Type attribute)
        {
            foreach (var attr in property.GetCustomAttributes(true))
            {
                if (attr.GetType() == attribute)
                {
                    return attr;
                }
            }

            return null;
        }

        private static IniData? ApplicationIniData;

        private static void InitApplicationIniData()
        {
            if (ApplicationIniData == null)
            {
                var parser = new FileIniDataParser();
                ApplicationIniData = parser.ReadFile("ConfigurationFile.ini");
            }
        }

        public static bool IsDesignMode => !File.Exists("ConfigurationFile.ini");

        public static string GetDatabaseConnectionString()
        {
            InitApplicationIniData();
            var ret = Helper.ApplicationIniData[SettingsCategory]["DatabaseConnectionString"];
            Debug.WriteLine(ret);
            return ret;
        }

        public static void SetUseDevelopment(bool useDevelopment)
        {
            if (useDevelopment)
            {
                SettingsCategory = "Development";
            }
            else
            {
                SettingsCategory = "Production";
            }
        }

        public static void FillCombo<Entity>(ComboBox combo, string sort = "id")
            where Entity : EntityBase, new()
        {
            using var repo = new Repository<Entity>();
            combo.Items.Clear();
            foreach (var item in repo.GetAll(sort))
            {
                combo.Items.Add(item);
            }
        }

        public static void SelectComboItem(ComboBox combo, EntityBase entity)
        {
            foreach (var item in combo.Items)
            {
                if (item is EntityBase obj && obj.Id == entity.Id)
                {
                    combo.SelectedItem = item;
                    break;
                }
            }
        }

        public static void SelectComboItem(ComboBox combo, int id)
        {
            foreach (var item in combo.Items)
            {
                if (item is EntityBase obj && obj.Id == id)
                {
                    combo.SelectedItem = item;
                    break;
                }
            }
        }

    }
}
