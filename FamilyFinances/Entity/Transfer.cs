﻿using ZefirLib.ORM;

namespace FamilyFinances.Entity
{
    public class Transfer : ZefirLib.ORM.EntityBase
    {
        [OneToMany(typeof(Wallet))]
        public int SourceWalletId { get; set; }
        [OneToMany(typeof(Wallet))]
        public int TargetWalletId { get; set; }
        public decimal Amount { get; set; }
        public DateTime EntryDate { get; set; }
    }
}
