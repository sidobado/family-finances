﻿namespace FamilyFinances.Entity
{
    public class Wallet : ZefirLib.ORM.EntityBase
    {
        public string Name { get; set; } = "";

        public override string ToString()
        {
            return Name;
        }

    }
}
