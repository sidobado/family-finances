﻿using ZefirLib.ORM;

namespace FamilyFinances.Entity
{
    public class ProductCategory : ZefirLib.ORM.EntityBase
    {
        [OneToMany(typeof(Product))]
        public int ProductId { get; set; }
        [OneToMany(typeof(Category))]
        public int CategoryId { get; set; }
    }
}
