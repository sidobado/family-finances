﻿namespace FamilyFinances.Entity
{
    public class Category : ZefirLib.ORM.EntityBase
    {
        public string Name { get; set; } = "";

        public override string ToString()
        {
            return Name;
        }
    }
}
