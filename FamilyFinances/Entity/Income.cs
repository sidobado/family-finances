﻿using ZefirLib.ORM;

namespace FamilyFinances.Entity
{
    public class Income : ZefirLib.ORM.EntityBase
    {
        public string Description { get; set; } = "";

        [OneToMany(typeof(Wallet))]
        public int WalletId { get; set; }

        [OneToMany(typeof(Salary))]
        public int? SalaryId { get; set; }

        public decimal Amount { get; set; }

        public DateTime EntryDate { get; set; }

        public override string ToString()
        {
            return Description + " (" + Amount.ToString() + ")";
        }
    }
}
