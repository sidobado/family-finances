﻿namespace FamilyFinances.Entity
{
    public class Budget : ZefirLib.ORM.EntityBase
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public override string ToString()
        {
            return StartDate.Year.ToString() + " - " + char.ToUpper(StartDate.ToString("MMMM")[0]) + StartDate.ToString("MMMM").Substring(1);
        }
    }
}
