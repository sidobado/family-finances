﻿using ZefirLib.ORM;

namespace FamilyFinances.Entity
{
    public class Transaction : ZefirLib.ORM.EntityBase
    {
        [OneToMany(typeof(Wallet))]
        public int WalletId { get; set; }
        [OneToMany(typeof(Place))]
        public int PlaceId { get; set; }
        [OneToMany(typeof(BudgetItem))]
        public int? BudgetItemId { get; set; } = null;
        [OneToMany(typeof(Product))]
        public int ProductId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Amount { get; set; }
        public DateTime EntryDate { get; set; }
        public bool IsInBudget { get; set; }
    }
}
