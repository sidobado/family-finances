﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZefirLib.ORM;

namespace FamilyFinances.Entity
{
    public class Salary: EntityBase
    {
        public string Name { get; set; } = "";
        public decimal Amount { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}
