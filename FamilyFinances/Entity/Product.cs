﻿using ZefirLib.ORM;

namespace FamilyFinances.Entity
{
    public class Product : ZefirLib.ORM.EntityBase
    {
        public string Name { get; set; } = "";

        [OneToMany(typeof(Unit))]
        public int UnitId { get; set; }

        [ManyToMany(typeof(Category), typeof(ProductCategory))]
        public ICollection<Category> CategoryId { get; set; } = new List<Category>();

        public override string ToString()
        {
            using var repo = new Repository<Unit>();
            var unit = repo.Get(UnitId);
            return Name + " (" + unit.ToString() + ")";
        }
    }
}
