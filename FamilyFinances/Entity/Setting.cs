﻿namespace FamilyFinances.Entity
{
    public class Setting : ZefirLib.ORM.EntityBase
    {
        public string Name { get; set; } = "";
        public string Value { get; set; } = "";
    }
}
