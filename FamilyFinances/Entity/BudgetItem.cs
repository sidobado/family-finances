﻿using ZefirLib.ORM;

namespace FamilyFinances.Entity
{
    public class BudgetItem : ZefirLib.ORM.EntityBase
    {
        [OneToMany(typeof(Budget))]
        public int BudgetId { get; set; }
        public string Description { get; set; } = "";
        public decimal Amount { get; set; }
        public override string ToString()
        {
            return Description;
        }
    }
}
