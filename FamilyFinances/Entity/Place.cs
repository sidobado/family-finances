﻿namespace FamilyFinances.Entity
{
    public class Place : ZefirLib.ORM.EntityBase
    {
        public string Name { get; set; } = "";
        public override string ToString()
        {
            return Name;
        }
    }
}
