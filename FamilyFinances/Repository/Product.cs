﻿using FamilyFinances.DTO;
using ZefirLib.ORM;

namespace FamilyFinances.Repository
{
    public class Product : Repository<Entity.Product>
    {
        public ProductWithTransactionDTO[] FindProductsWithLastTransaction(string text)
        {
            var transactionRepo = new Transaction();
            var result = new List<ProductWithTransactionDTO>();

            using var command = Database.CreateCommand();
            string tag = "$T" + DateTime.Now.Millisecond.ToString() + "$";
            command.CommandText = $"SELECT * FROM Product WHERE name ILIKE {tag}%{text}%{tag} order by name limit 4";
            var products = GetAll(command);

            foreach (var product in products)
            {
                var transaction = transactionRepo.GetLastByProduct(product);
                result.Add(new ProductWithTransactionDTO { Product = product, Transaction = transaction });
            }

            return result.ToArray();
        }
    }
}
