﻿using ZefirLib.ORM;

namespace FamilyFinances.Repository
{
    public class Budget : Repository<Entity.Budget>
    {
        public Entity.Budget? GetByDate(DateTime date)
        {
            using var command = Database.CreateCommand();
            command.CommandText = "SELECT * FROM budget WHERE @date BETWEEN startdate AND enddate";
            command.Parameters.AddWithValue("@date", date);

            return Get(command);
        }

        public Entity.Budget? GetNewest()
        {
            using var command = Database.CreateCommand();
            command.CommandText = "SELECT * FROM budget ORDER BY enddate DESC LIMIT 1";
            return Get(command);
        }

        public Entity.Budget? GetNextBudget(Entity.Budget prevBudget)
        {
            using var command = Database.CreateCommand();
            command.CommandText = "SELECT * FROM budget WHERE id > @id ORDER BY startdate LIMIT 1";
            command.Parameters.AddWithValue("@id", prevBudget.Id);

            return Get(command);
        }
    }
}
