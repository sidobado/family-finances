﻿using FamilyFinances.DTO;
using FamilyFinances.Entity;
using ZefirLib.ORM;

namespace FamilyFinances.Repository
{
    public class Raport
    {
        public WalletBalanceDTO[] GetWalletsBalance(DateTime date)
        {
            List<WalletBalanceDTO> result = new();

            using var command = Database.CreateCommand();
            command.CommandText =
                "select " +
                "*, " +
                "(" +
                    "coalesce((select sum(amount) from income where walletid=w.id and entrydate<=@date), 0)::numeric - " +
                    "coalesce((select sum(amount) from transaction where walletid=w.id and entrydate<=@date), 0)::numeric +" +
                    "coalesce((select sum(amount) from transfer t where targetwalletid=w.id and entrydate<=@date), 0)::numeric -" +
                    "coalesce((select sum(amount) from transfer t where sourcewalletid=w.id and entrydate<=@date), 0)::numeric " +
                ")::numeric as amount " +
                "from wallet w " +
                "order by amount desc ";
            command.Parameters.AddWithValue("@date", date);
            command.Prepare();

            using var reader = command.ExecuteReader();
            var repo = new Repository<Wallet>();
            while (reader.Read())
            {
                result.Add(new WalletBalanceDTO
                {
                    Balance = (decimal)reader["amount"],
                    Wallet = repo.Get((int)reader["id"]) ?? new()
                });
            }

            return result.ToArray();
        }

        public decimal GetTotalBalance(DateTime date)
        {
            List<WalletBalanceDTO> result = new();

            using var command = Database.CreateCommand();
            command.CommandText =
                "select " +
                "(" +
                    "coalesce((select sum(amount) from income where entrydate<=@date), 0)::numeric - " +
                    "coalesce((select sum(amount) from transaction where entrydate<=@date), 0)::numeric " +
                ")::numeric as amount;";

            command.Parameters.AddWithValue("@date", date);
            command.Prepare();

            return (decimal)(command.ExecuteScalar() ?? 0.0m);
        }

        public decimal GetUnplannedExpenses(DateTime dateFrom, DateTime dateTo)
        {
            using var command = Database.CreateCommand();
            command.CommandText =
                "select " +
                "coalesce(sum(amount), 0)::numeric " +
                "from transaction " +
                "where entrydate between @dateFrom and @dateTo and not isinbudget";
            command.Parameters.AddWithValue("@dateFrom", dateFrom);
            command.Parameters.AddWithValue("@dateTo", dateTo);
            command.Prepare();
            return (decimal)(command.ExecuteScalar() ?? 0.0m);
        }

        public decimal GetPlannedExpenses(DateTime dateFrom, DateTime dateTo)
        {
            using var command = Database.CreateCommand();
            command.CommandText =
                "select " +
                "coalesce(sum(amount), 0)::numeric " +
                "from transaction " +
                "where entrydate between @dateFrom and @dateTo and isinbudget";
            command.Parameters.AddWithValue("@dateFrom", dateFrom);
            command.Parameters.AddWithValue("@dateTo", dateTo);
            command.Prepare();
            return (decimal)(command.ExecuteScalar() ?? 0.0m);
        }

        public decimal GetPlannedFutureExpenses(Entity.Budget budget, DateTime dateFrom, DateTime dateTo)
        {
            using var command = Database.CreateCommand();
            command.CommandText =
                "select coalesce(sum(amount), 0)::numeric " +
                "from budgetitem b  " +
                "where b.budgetid=@budgetId and " +
                "not exists(select from transaction where budgetitemid=b.id and entrydate between @dateFrom and @dateTo)";
            command.Parameters.AddWithValue("@budgetId", budget.Id);
            command.Parameters.AddWithValue("@dateFrom", dateFrom);
            command.Parameters.AddWithValue("@dateTo", dateTo);
            command.Prepare();
            return (decimal)(command.ExecuteScalar() ?? 0.0m);
        }

        public decimal GetBudgetItemExpenses(Entity.BudgetItem budgetItem)
        {
            using var command = Database.CreateCommand();
            command.CommandText =
                "select coalesce(sum(amount), 0)::numeric " +
                "from transaction " +
                "where budgetitemid=@budgetItemId";
            command.Parameters.AddWithValue("@budgetItemId", budgetItem.Id);
            command.Prepare();
            return (decimal)(command.ExecuteScalar() ?? 0.0m);
        }

        public decimal GetIncomes(DateTime dateFrom, DateTime dateTo)
        {
            using var command = Database.CreateCommand();
            command.CommandText =
                "select coalesce(sum(amount), 0)::numeric " +
                "from income " +
                "where entrydate between @dateFrom and @dateTo";
            command.Parameters.AddWithValue("@dateFrom", dateFrom);
            command.Parameters.AddWithValue("@dateTo", dateTo);
            command.Prepare();
            return (decimal)(command.ExecuteScalar() ?? 0.0m);
        }

        public decimal GetFutureIncomes(DateTime dateFrom, DateTime dateTo)
        {
            using var command = Database.CreateCommand();
            command.CommandText =
                "select coalesce(sum(amount), 0)::numeric " +
                "from salary s " +
                "where not exists(select from income i where s.id=i.salaryid and entrydate between @dateFrom and @dateTo)";
            command.Parameters.AddWithValue("@dateFrom", dateFrom);
            command.Parameters.AddWithValue("@dateTo", dateTo);
            command.Prepare();
            return (decimal)(command.ExecuteScalar() ?? 0.0m);
        }
    }
}
