﻿using ZefirLib.ORM;

namespace FamilyFinances.Repository
{
    public class BudgetItem : Repository<Entity.BudgetItem>
    {
        public Entity.BudgetItem[] GetItems(Entity.Budget budget)
        {
            var command = Database.CreateCommand();
            command.CommandText = "SELECT * FROM budgetitem WHERE budgetid = @budgetid";
            command.Parameters.AddWithValue("@budgetid", budget.Id);
            return GetAll(command);
        }
    }
}
