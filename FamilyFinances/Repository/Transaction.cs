﻿using FamilyFinances.DTO;
using FamilyFinances.Entity;
using ZefirLib.ORM;

namespace FamilyFinances.Repository
{
    public class Transaction : Repository<Entity.Transaction>
    {
        public InvoiceDTO[] GetInvoiceList(DateTime dateFrom, DateTime dateTo)
        {
            var ret = new List<InvoiceDTO>();

            var walletRepo = new Repository<Wallet>();
            var placeRepo = new Repository<Place>();

            string query = "select " +
                "   t.entrydate, w.id as walletid, p.id as placeid, coalesce(b.description, '') as budgetname, " +
                "   sum(t.amount) as amount, array_agg(t.id) as transactions" +
                " from transaction t" +
                " join wallet w on w.id=t.walletid" +
                " join place p on p.id=placeid" +
                " left join budgetitem b on b.id=t.budgetitemid" +
                " where t.entrydate between @dateFrom and @dateTo" +
                " group by t.entrydate, p.id, w.id, b.id" +
                " order by t.entrydate desc";

            using var command = Database.CreateCommand();
            command.CommandText = query;
            command.Parameters.AddWithValue("@dateFrom", dateFrom);
            command.Parameters.AddWithValue("@dateTo", dateTo);
            command.Prepare();

            using var reader = command.ExecuteReader();
            while (reader.Read())
            {
#pragma warning disable CS8604 // Possible null reference argument wont happen - joins in query prevent this
                var dto = new InvoiceDTO(
                    date: (DateTime)reader["entrydate"],
                    place: placeRepo.Get(id: (int)reader["placeid"]),
                    wallet: walletRepo.Get(id: (int)reader["walletid"]),
                    amount: (decimal)reader["amount"],
                    budgetName: (string)reader["budgetname"],
                    transactionIds: (int[])reader["transactions"]
                    );
#pragma warning restore CS8604 // Possible null reference argument wont happen - joins in query prevent this
                ret.Add(dto);
            }

            return ret.ToArray();
        }

        public Entity.Transaction? GetLastByProduct(Entity.Product product)
        {
            using var command = Database.CreateCommand();
            command.CommandText = "SELECT * FROM transaction WHERE productid=@productid ORDER BY entrydate DESC LIMIT 1";
            command.Parameters.AddWithValue("@productid", product.Id);

            return Get(command);
        }
    }
}
