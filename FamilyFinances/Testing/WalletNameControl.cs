﻿using FamilyFinances.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZefirLib.ORM;

namespace FamilyFinances.Testing
{
    public class WalletNameControl: BaseNameControl
    {
        protected override void InitializeGenericComponent()
        {
            EntityType = typeof(Wallet);
            Repository = new Repository<Wallet>();
        }
    }
}
