﻿namespace FamilyFinances.Testing
{
    partial class SpecialForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            categoryNameControl1 = new CategoryNameControl();
            walletNameControl1 = new WalletNameControl();
            SuspendLayout();
            // 
            // categoryNameControl1
            // 
            categoryNameControl1.Location = new Point(12, 12);
            categoryNameControl1.Name = "categoryNameControl1";
            categoryNameControl1.Size = new Size(172, 68);
            categoryNameControl1.TabIndex = 0;
            // 
            // walletNameControl1
            // 
            walletNameControl1.Location = new Point(369, 61);
            walletNameControl1.Name = "walletNameControl1";
            walletNameControl1.Size = new Size(215, 150);
            walletNameControl1.TabIndex = 1;
            // 
            // SpecialForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(walletNameControl1);
            Controls.Add(categoryNameControl1);
            Name = "SpecialForm";
            Text = "SpecialForm";
            ResumeLayout(false);
        }

        #endregion

        private CategoryNameControl categoryNameControl1;
        private WalletNameControl walletNameControl1;
    }
}