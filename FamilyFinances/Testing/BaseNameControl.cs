﻿using System.ComponentModel;
using ZefirLib.ORM;

namespace FamilyFinances.Testing
{
    public partial class BaseNameControl : UserControl
    {
        public bool designMode = (LicenseManager.UsageMode == LicenseUsageMode.Designtime);
        public BaseNameControl()
        {
            InitializeComponent();

            //bool designMode = System.Diagnostics.Process.GetCurrentProcess().ProcessName == "devenv";
            if (!designMode)
            {
                Load += BaseNameControl_Load;
            }
        }

        protected Type? EntityType;
        protected dynamic? Entity;
        protected dynamic? Repository = null;

        private void BaseNameControl_Load(object? sender, EventArgs e)
        {
            if (!designMode)
            {
                InitializeGenericComponent();
                MyInit();
                Reload();
            }
        }

        private void Reload()
        {
            if (Repository is null || designMode)
            { return; }
            genericName.Text = Entity?.Name;
        }

        private void MyInit()
        {
            if (Repository is null || designMode)
            { return; }
            Entity = Repository.Get(1);
        }

        protected virtual void InitializeGenericComponent()
        {
        }
    }
}
