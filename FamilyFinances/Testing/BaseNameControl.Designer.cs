﻿namespace FamilyFinances.Testing
{
    partial class BaseNameControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            genericName = new Label();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(17, 23);
            label1.Name = "label1";
            label1.Size = new Size(42, 15);
            label1.TabIndex = 0;
            label1.Text = "Name:";
            // 
            // genericName
            // 
            genericName.AutoSize = true;
            genericName.Location = new Point(74, 23);
            genericName.Name = "genericName";
            genericName.Size = new Size(42, 15);
            genericName.TabIndex = 0;
            genericName.Text = "Name:";
            // 
            // BaseNameControl
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(genericName);
            Controls.Add(label1);
            Name = "BaseNameControl";
            Size = new Size(215, 150);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        protected Label genericName;
    }
}
