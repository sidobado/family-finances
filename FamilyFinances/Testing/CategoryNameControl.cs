﻿using FamilyFinances.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZefirLib.ORM;

namespace FamilyFinances.Testing
{
    public class CategoryNameControl: BaseNameControl
    {
        protected override void InitializeGenericComponent()
        {
            EntityType = typeof(Category);
            Repository = new Repository<Category>();
        }
    }
}
