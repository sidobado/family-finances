﻿using FamilyFinances.Entity;
using System.Diagnostics;
using ZefirLib.ORM;

#pragma warning disable CS8618

namespace FamilyFinances
{
    public class Fixtures
    {
        public void Run()
        {
            SaveUnits();
            SaveCategories();
            SaveProducts();
            SavePlaces();
            SaveWallets();
            SaveTransfers();
            SaveSalaries();
            SaveIncomes();
            SaveBudgets();
            SaveBudgetItems();
            SaveTransactions();
            Debug.WriteLine("Fixtures saved");
        }

        #region Units
        private Unit UnitLitr;
        private readonly List<Unit> Units = new();

        private void SaveUnits()
        {
            UnitLitr = new() { Name = "Litr" };
            Units.Add(UnitLitr);

            SaveList(Units);
        }
        #endregion

        #region Categories
        private Category CategoryNapoje;
        private Category CategorySpozywcze;
        private readonly List<Category> Categories = new();

        private void SaveCategories()
        {
            CategoryNapoje = new() { Name = "Napoje" };
            CategorySpozywcze = new() { Name = "Spożywcze" };

            Categories.Add(CategoryNapoje);
            Categories.Add(CategorySpozywcze);

            SaveList(Categories);
        }
        #endregion

        #region Products
        private Product ProductCola;
        private Product ProductPepsi;
        private Product ProductMleko;
        private readonly List<Product> Products = new();

        private void SaveProducts()
        {
            ProductCola = new()
            {
                Name = "Coca Cola",
                UnitId = UnitLitr.Id,
                CategoryId = new List<Category>()
            };
            ProductCola.CategoryId.Add(CategoryNapoje);

            ProductPepsi = new()
            {
                Name = "Pepsi",
                UnitId = UnitLitr.Id,
                CategoryId = new List<Category>()
            };
            ProductPepsi.CategoryId.Add(CategoryNapoje);

            ProductMleko = new()
            {
                Name = "Mleko",
                UnitId = UnitLitr.Id,
                CategoryId = new List<Category>()
            };
            ProductMleko.CategoryId.Add(CategorySpozywcze);

            Products.Add(ProductCola);
            Products.Add(ProductPepsi);
            Products.Add(ProductMleko);

            SaveList(Products);
        }
        #endregion

        #region Places
        private Place PlaceBiedra;
        private Place PlaceLidl;
        private readonly List<Place> Places = new();

        private void SavePlaces()
        {
            PlaceBiedra = new() { Name = "Biedronka" };
            PlaceLidl = new() { Name = "Lidl" };

            Places.Add(PlaceBiedra);
            Places.Add(PlaceLidl);

            SaveList(Places);
        }
        #endregion

        #region Wallets
        private Wallet WalletPrzyDupie;
        private Wallet WalletKonto;
        private readonly List<Wallet> Wallets = new();

        private void SaveWallets()
        {
            WalletPrzyDupie = new() { Name = "Przy dupie" };
            WalletKonto = new() { Name = "eKonto" };

            Wallets.Add(WalletPrzyDupie);
            Wallets.Add(WalletKonto);

            SaveList(Wallets);
        }
        #endregion

        #region Transfers
        private readonly List<Transfer> Transfers = new();
        private Transfer Bankomat;

        private void SaveTransfers()
        {
            Bankomat = new()
            {
                SourceWalletId = WalletKonto.Id,
                TargetWalletId = WalletPrzyDupie.Id,
                Amount = 150,
                EntryDate = DateTime.UtcNow,
            };
            Transfers.Add(Bankomat);
            SaveList(Transfers);
        }
        #endregion

        #region Salaries
        private readonly List<Salary> Salaries = new();
        private Salary SalaryPraca;

        private void SaveSalaries()
        {
            SalaryPraca = new()
            {
                Name = "Zefir",
                Amount = 3000,
            };
            Salaries.Add(SalaryPraca);
            SaveList(Salaries);
        }
        #endregion

        #region Incomes
        private readonly List<Income> Incomes = new();
        private Income IncomePraca;
        private Income IncomeLotto;

        private void SaveIncomes()
        {
            IncomePraca = new()
            {
                WalletId = WalletKonto.Id,
                SalaryId = SalaryPraca.Id,
                Amount = 5200,
                EntryDate = DateTime.UtcNow,
                Description = "Wypłata - " + DateTime.UtcNow.ToString("MMMM"),
            };
            IncomeLotto = new()
            {
                WalletId = WalletKonto.Id,
                Amount = 1000,
                EntryDate = DateTime.UtcNow,
                Description = "Wygrana w lotka",
            };
            Incomes.Add(IncomePraca);
            Incomes.Add(IncomeLotto);
            SaveList(Incomes);
        }
        #endregion

        #region Budget
        private readonly List<Budget> Budgets = new();
        private Budget BudgetCurrent;

        private void SaveBudgets()
        {
            int beginyear = DateTime.UtcNow.Year;
            int beginmonth = DateTime.UtcNow.Month;
            int beginday = 1;

            int endyear = DateTime.UtcNow.Year;
            int endmonth = DateTime.UtcNow.Month + 1;
            int endday = 1;

            if (endmonth > 12)
            {
                endmonth = 1;
                endyear++;
            }

            DateTime begin = new(beginyear, beginmonth, beginday);
            DateTime end = new(endyear, endmonth, endday);
            end = end.AddSeconds(-1);
            BudgetCurrent = new()
            {
                StartDate = begin,
                EndDate = end,
            };
            Budgets.Add(BudgetCurrent);
            SaveList(Budgets);
        }
        #endregion

        #region BudgetItems
        private readonly List<BudgetItem> BudgetItems = new();
        private BudgetItem BudgetItemCola;
        private BudgetItem BudgetItemNetflix;

        private void SaveBudgetItems()
        {
            BudgetItemCola = new()
            {
                BudgetId = BudgetCurrent.Id,
                Description = "Kolawka",
                Amount = 14.99m,
            };

            BudgetItemNetflix = new()
            {
                BudgetId = BudgetCurrent.Id,
                Description = "Netflix",
                Amount = 59.99m,
            };

            BudgetItems.Add(BudgetItemCola);
            BudgetItems.Add(BudgetItemNetflix);

            SaveList(BudgetItems);
        }
        #endregion

        #region Transactions
        private readonly List<Transaction> Transactions = new();
        private Transaction TransactionCola;
        private Transaction TransactionCola2;
        private Transaction TransactionPepsi;

        private void SaveTransactions()
        {
            TransactionCola = new()
            {
                WalletId = WalletKonto.Id,
                PlaceId = PlaceBiedra.Id,
                ProductId = ProductCola.Id,
                Quantity = 2,
                Amount = 14.99m,
                EntryDate = DateTime.UtcNow,
                IsInBudget = true,
                BudgetItemId = BudgetItemCola.Id,
            };

            TransactionCola2 = new()
            {
                WalletId = WalletKonto.Id,
                PlaceId = PlaceLidl.Id,
                ProductId = ProductCola.Id,
                Quantity = 2,
                Amount = 14.99m,
                EntryDate = DateTime.UtcNow,
                IsInBudget = false,
                BudgetItemId = null,
            };

            TransactionPepsi = new()
            {
                WalletId = WalletKonto.Id,
                PlaceId = PlaceLidl.Id,
                ProductId = ProductPepsi.Id,
                Quantity = 2,
                Amount = 14.99m,
                EntryDate = DateTime.UtcNow,
                IsInBudget = false,
                BudgetItemId = null,
            };

            Transactions.Add(TransactionCola);
            Transactions.Add(TransactionCola2);
            Transactions.Add(TransactionPepsi);

            SaveList(Transactions);
        }
        #endregion

        private static void SaveList<T>(List<T> list) where T : EntityBase, new()
        {
            var repository = new Repository<T>();
            foreach (var item in list)
            {
                repository.Save(item);
            }
        }
    }
}
