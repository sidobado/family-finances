Usage:


### How to regenerate manifest.txt file for installer

1. Publish listed below projects into same directory, (example: D:\Projects\Publish\FamilyFinances):
	- FamilyInstances
	- Installer
2. Create shortcut of Installer.exe in published directory
3. Edit created shortcut's properties and add following arguments to target:
	- /i "FamilyInstances.dll"
	- /t "FamilyInstances.dll"