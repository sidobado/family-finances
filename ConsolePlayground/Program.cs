﻿// See https://aka.ms/new-console-template for more information

using ZefirLib.Reg;

internal class Program
{
    private static void Main(string[] args)
    {
        var key = Registry.OpenOrCreateKey(
            Registry.RegistryRoot.LocalMachine,
            "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall",
            "FamilyFinances"
            );

        key.Set<string>("foo", "bar");
        string? iconPath = key.Get<string?>("DisplayIcon");

        Console.WriteLine(iconPath);

        Console.WriteLine("Hello, World!");
    }
}