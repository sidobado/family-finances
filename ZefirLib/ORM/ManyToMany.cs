﻿namespace ZefirLib.ORM
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ManyToMany : Attribute
    {
        public Type Entity { get; }
        public Type CollectionEnity { get; }

        public ManyToMany(Type entity, Type collectionEnity)
        {
            Entity = entity;
            CollectionEnity = collectionEnity;
        }
    }
}
