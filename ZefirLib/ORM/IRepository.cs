﻿namespace ZefirLib.ORM
{
    public interface IRepository<Entity>
    {
        public string TableName { get; }
        public Entity[] GetAll();

        public Entity? Get(int id);

        public Entity Save(Entity entity);

        public void Delete(Entity entity);
    }
}
