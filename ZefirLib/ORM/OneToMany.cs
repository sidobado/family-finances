﻿namespace ZefirLib.ORM
{
    [AttributeUsage(AttributeTargets.Property)]
    public class OneToMany : Attribute
    {
        public Type Entity { get; }

        public OneToMany(Type entity)
        {
            if (false == entity.IsSubclassOf(typeof(EntityBase)))
            {
                throw new ArgumentException("Parameter 'entity' have to be subclass of EntityBase");
            }
            Entity = entity;
        }
    }
}
