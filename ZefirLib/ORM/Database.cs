﻿using Npgsql;
using System.Data;
using System.Diagnostics;

namespace ZefirLib.ORM
{
    public class Database
    {
        private static readonly List<NpgsqlConnection> Connections = new();
        private static string ConnectionsString = "";

        public static void SetConnectionString(string connectionString) => ConnectionsString = connectionString;

        public static NpgsqlCommand CreateCommand()
        {
            NpgsqlConnection? connection = FindFreeConnection();
            if (connection is null)
            {
                Debug.WriteLine("No free connections, creating new one, after create there will be " + (Connections.Count + 1) + " connections.");
                connection = new NpgsqlConnection(ConnectionsString);
                connection.Open();
                Connections.Add(connection);
            }

            return connection.CreateCommand();
        }

        private static NpgsqlConnection? FindFreeConnection()
        {
            foreach (var connection in Connections)
            {
                bool isFetching = ((int)(connection.FullState & ConnectionState.Fetching)) > 0;
                if (isFetching == false)
                {
                    return connection;
                }
            }

            return null;
        }

        public static bool IsTableExists(string tableName)
        {
            using var cmd = CreateCommand();
            cmd.CommandText = "SELECT EXISTS (SELECT FROM information_schema.tables WHERE table_schema = 'public' AND table_name = @tableName);";
            cmd.Parameters.AddWithValue("tableName", tableName);
            var ret = cmd.ExecuteScalar();
            return (bool)(ret ?? false);
        }

    }
}
