﻿using Npgsql;
using System.Collections;
using System.Data;
using System.Reflection;

namespace ZefirLib.ORM
{
    public class Repository<Entity> : IRepository<Entity>, IDisposable
        where Entity : EntityBase, new()
    {
        public Repository()
        {

        }

        private static readonly Dictionary<int, Entity?> _cache = new();

        private string _table = "";
        public string TableName
        {
            get
            {
                if (_table == "")
                {
                    var x = new Entity();
                    _table = x.GetType().Name;
                }
                return _table;
            }
        }

        public virtual Entity[] GetAll()
        {
            return GetAll("id", 0);
        }

        public Entity[] GetAll(string sortBy)
        {
            return GetAll(sortBy, 0);
        }

        public Entity[] GetAll(string sortBy, uint limit)
        {
            using var command = Database.CreateCommand();
            command.CommandText = "Select * From " + TableName + " Order By " + sortBy;

            if (limit > 0)
            {
                command.CommandText += " Limit " + limit.ToString();
            }

            return GetAll(command);
        }

        public Entity[] GetAllByIds(int[] ids)
        {
            return GetAllByIds(ids, "id asc");
        }

        public Entity[] GetAllByIds(int[] ids, string sortBy)
        {
            using var command = Database.CreateCommand();
            command.CommandText = "Select * From " + TableName + " Where id = ANY(@ids) Order By " + sortBy;
            command.Parameters.AddWithValue("@ids", ids);
            return GetAll(command);
        }

        protected Entity[] GetAll(NpgsqlCommand command)
        {
            List<Entity> list = new();

            if (command.IsPrepared == false)
            {
                command.Prepare();
            }

            using var reader = command.ExecuteReader();
            while (reader.Read())
            {
                var item = new Entity();
                foreach (var prop in item.GetType().GetProperties())
                {
                    var attr = (ManyToMany?)FindAttributeInProperty(prop, typeof(ManyToMany));
                    if (attr is null)
                    {
                        var name = prop.Name;
                        var value = reader[name];
                        if (value == DBNull.Value)
                        {
                            value = null;
                        }

                        prop.SetValue(item, value);
                    }
                    else
                    {
                        Type myGeneric = typeof(Repository<>);
                        Type constructedClass = myGeneric.MakeGenericType(attr.CollectionEnity);
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
                        dynamic instance = Activator.CreateInstance(constructedClass);
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.

                        MethodInfo? method = constructedClass?.GetMethod("GetEntityColumnByField");
                        MethodInfo? generic = method?.MakeGenericMethod(attr.Entity);

                        var returnValue = generic?.Invoke(instance, new object[] { prop.Name, (typeof(Entity)).Name + "Id", (int)reader["id"] });
                        prop.SetValue(item, returnValue);
                    }
                }
                list.Add(item);
                _cache[item.Id] = item;
            }

            return list.ToArray();
        }

        protected Entity? Get(NpgsqlCommand command)
        {
            if (command.IsPrepared == false)
            {
                command.Prepare();
            }
            using var reader = command.ExecuteReader();
            if (reader.Read())
            {
                var item = new Entity();
                foreach (var prop in item.GetType().GetProperties())
                {
                    var attr = (ManyToMany?)FindAttributeInProperty(prop, typeof(ManyToMany));
                    if (attr is null)
                    {
                        var value = reader[prop.Name];
                        if (value == DBNull.Value)
                        {
                            value = null;
                        }
                        prop.SetValue(item, value);
                    }
                    else
                    {
                        Type myGeneric = typeof(Repository<>);
                        Type constructedClass = myGeneric.MakeGenericType(attr.CollectionEnity);
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
                        dynamic instance = Activator.CreateInstance(constructedClass);
                        MethodInfo method = constructedClass.GetMethod("GetEntityColumnByField");
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.

                        MethodInfo generic = method.MakeGenericMethod(attr.Entity);

                        var returnValue = generic.Invoke(instance, new object[] { prop.Name, (typeof(Entity)).Name + "Id", (int)reader["id"] });
                        prop.SetValue(item, returnValue);
                    }
                }
                return item;
            }

            return null;
        }

        public Entity? Get(int id)
        {
            if (_cache.ContainsKey(id))
            {
                return _cache[id];
            }
            using var command = Database.CreateCommand();
            command.CommandText = "Select * From " + TableName + " WHERE Id=@Id";
            command.Parameters.AddWithValue("@Id", id);
            return _cache[id] = Get(command);
        }


        public T[] GetEntityColumnByField<T>(string entityColumn, string field, object value) where T : EntityBase, new()
        {
            List<T> ret = new();

            using var command = Database.CreateCommand();
            string qField = "@" + field;
            command.CommandText = "Select * From " + TableName + " Where " + field + "=" + qField;
            command.Parameters.AddWithValue(qField, value);
            command.Prepare();

            using var reader = command.ExecuteReader();
            while (reader.Read())
            {
                var properties = (typeof(Entity)).GetProperties();
                foreach (var prop in properties)
                {
                    if (prop.Name == field)
                    {
                        var attr = (OneToMany?)FindAttributeInProperty(prop, typeof(OneToMany));
                        if (attr is null)
                        {
                            throw new Exception("Something went wrong; todo fix this message");
                        }
                        Type myGeneric = typeof(Repository<>);
                        Type constructedClass = myGeneric.MakeGenericType(typeof(T));
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
                        dynamic instance = Activator.CreateInstance(constructedClass);
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.
                        var item = instance.Get((int)reader[entityColumn]);
                        ret.Add(item);
                    }
                }
            }

            return ret.ToArray();
        }

        public Entity Save(Entity entity)
        {
            if (entity.Id == 0)
            {
                Insert(entity);
                return _cache[entity.Id] = entity;
            }
            else
            {
                return _cache[entity.Id] = Update(entity);
            }
        }

        private Entity Insert(Entity entity)
        {
            List<string> fields = new();
            List<string> values = new();
            bool hasManyToMany = false;

            PropertyInfo[] properties = entity.GetType().GetProperties();
            for (int i = 0; i < properties.Length; i++)
            {
                PropertyInfo prop = properties[i];

                if (prop.Name == "Id") continue;
                if (FindAttributeInProperty(prop, typeof(ManyToMany)) != null)
                {
                    hasManyToMany = true;
                    continue;
                }

                fields.Add(prop.Name);
                values.Add("@" + prop.Name);
            }
            string fieldsQuery = string.Join(", ", fields);
            string valuesQuery = string.Join(", ", values);
            string query = "Insert Into " + TableName + " (" + fieldsQuery + ") Values (" + valuesQuery + ") Returning id";

            using (var command = Database.CreateCommand())
            {
                command.CommandText = query;
                for (int i = 0; i < properties.Length; i++)
                {
                    PropertyInfo prop = properties[i];
                    if (prop.Name == "Id") continue;
                    if (FindAttributeInProperty(prop, typeof(ManyToMany)) != null)
                    {
                        continue;
                    }
                    var value = prop.GetValue(entity);
                    value ??= DBNull.Value;
                    command.Parameters.AddWithValue("@" + prop.Name, value);
                }

                using var reader = command.ExecuteReader();
                if (reader.Read())
                {
                    entity.Id = reader.GetInt32(0);
                }
            }

            if (hasManyToMany)
            {
                InsertManyToMany(entity);
            }

            return entity;
        }

        private void InsertManyToMany(Entity entity)
        {
            PropertyInfo[] properties = entity.GetType().GetProperties();
            string deleteByField = entity.GetType().Name + "Id";
            object deleteByValue = entity.Id;

            for (int i = 0; i < properties.Length; i++)
            {
                PropertyInfo prop = properties[i];
                var attr = (ManyToMany?)FindAttributeInProperty(prop, typeof(ManyToMany));
                if (attr is not null)
                {
                    Type myGeneric = typeof(Repository<>);
                    Type constructedClass = myGeneric.MakeGenericType(attr.CollectionEnity);
                    dynamic instance = Activator.CreateInstance(constructedClass);

                    MethodInfo DeleteByFieldMethod = constructedClass.GetMethod("DeleteByField");
                    MethodInfo InsertMethod = constructedClass.GetMethod("Save");

                    DeleteByFieldMethod.Invoke(instance, new object[] { deleteByField, deleteByValue });

                    var tempItems = prop.GetValue(entity, null);
                    var tempItems2 = tempItems as IEnumerable;

                    foreach (var element in tempItems2)
                    {
                        var item = element as EntityBase;
                        dynamic entityCollection = Activator.CreateInstance(attr.CollectionEnity);
                        PropertyInfo firstId = entityCollection.GetType().GetProperty(entity.GetType().Name + "Id");
                        firstId.SetValue(entityCollection, entity.Id, null);
                        PropertyInfo secondId = entityCollection.GetType().GetProperty(attr.Entity.Name + "Id");
                        secondId.SetValue(entityCollection, item.Id, null);

                        InsertMethod.Invoke(instance, new object[] { entityCollection });
                    }
                }
            }
        }

        private Entity Update(Entity entity)
        {
            List<string> fields = new();
            bool hasManyToMany = false;

            PropertyInfo[] properties = entity.GetType().GetProperties();
            for (int i = 0; i < properties.Length; i++)
            {
                PropertyInfo? prop = properties[i];
                if (prop.Name == "Id") continue;
                if (FindAttributeInProperty(prop, typeof(ManyToMany)) != null)
                {
                    hasManyToMany = true;
                    continue;
                }
                fields.Add(prop.Name + "=@" + prop.Name);
            }
            string fieldsQuery = string.Join(", ", fields);
            string query = "Update " + TableName + " Set " + fieldsQuery + " Where id=@Id";

            using var command = Database.CreateCommand();
            command.CommandText = query;
            for (int i = 0; i < properties.Length; i++)
            {
                PropertyInfo? prop = properties[i];
                if (FindAttributeInProperty(prop, typeof(ManyToMany)) != null)
                {
                    continue;
                }
#pragma warning disable CS8604 // Possible null reference argument.
                var value = prop.GetValue(entity);
                if (value == null)
                {
                    value = DBNull.Value;
                }
                command.Parameters.AddWithValue("@" + prop.Name, value);
#pragma warning restore CS8604 // Possible null reference argument.
            }
            command.ExecuteNonQuery();

            if (hasManyToMany)
            {
                InsertManyToMany(entity);
            }

            return entity;
        }

        public void Delete(Entity entity)
        {
            PropertyInfo[] properties = entity.GetType().GetProperties();
            for (int i = 0; i < properties.Length; i++)
            {
                PropertyInfo prop = properties[i];
                var attr = (ManyToMany?)FindAttributeInProperty(prop, typeof(ManyToMany));
                if (attr is not null)
                {
                    Type myGeneric = typeof(Repository<>);
                    Type constructedClass = myGeneric.MakeGenericType(attr.CollectionEnity);
                    dynamic instance = Activator.CreateInstance(constructedClass);

                    MethodInfo DeleteByFieldMethod = constructedClass.GetMethod("DeleteByField");
                    MethodInfo InsertMethod = constructedClass.GetMethod("Save");

                    string deleteByField = entity.GetType().Name + "Id";
                    object deleteByValue = entity.Id;

                    DeleteByFieldMethod.Invoke(instance, new object[] { deleteByField, deleteByValue });
                }
            }

            _cache.Remove(entity.Id);
            DeleteByField("id", entity.Id);
        }

        public void DeleteByField(string field, object value)
        {
            using var command = Database.CreateCommand();
            command.CommandText = "Delete From " + TableName + " WHERE " + field + "=@value";
            command.Parameters.AddWithValue("@value", value);
            command.ExecuteNonQuery();
        }

        protected bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }

        public void Dispose()
        {
        }

        public static object? FindAttributeInProperty(PropertyInfo property, Type attribute)
        {
            foreach (var attr in property.GetCustomAttributes(true))
            {
                if (attr.GetType() == attribute)
                {
                    return attr;
                }
            }

            return null;
        }
    }
}
