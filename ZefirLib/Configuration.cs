﻿using IniParser;
using IniParser.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZefirLib
{
    public class Configuration
    {
        private static Configuration? instance = null;

        public static Configuration Instance(string fileName)
        {
            if (instance == null)
            {
                instance = new(fileName);
            }

            return instance;
        }

        public string Filename { get; private set; }
        private readonly IniData? ApplicationIniData;

        private Configuration(string fileName)
        {
            Filename = fileName;
            var parser = new FileIniDataParser();
            ApplicationIniData = parser.ReadFile(Filename);
        }

        public string Get(string groupName, string keyName)
        {
            var group = ApplicationIniData[groupName];
            if (group != null)
            {
                string? val = group[keyName];
                return val ?? string.Empty;
            }
            return string.Empty;
        }
    }
}
