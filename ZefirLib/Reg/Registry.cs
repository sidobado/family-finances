﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using R = Microsoft.Win32;

#pragma warning disable CA1416 // Validate platform compatibility -> App is only for Windows
namespace ZefirLib.Reg
{
    public sealed class Registry : IDisposable
    {
        public enum RegistryRoot
        {
            ClassesRoot,
            CurrentUser,
            LocalMachine,
            Users,
            CurrentConfig
        }

        public static Registry OpenOrCreateKey(RegistryRoot root, string path, string keyName)
        {
            var key = GetRootKey(root);
            var subkey = key.OpenSubKey(path, true) ?? throw new ArgumentException("Provided path to keyName does not exists: " + path);
            var targetKey = subkey.OpenSubKey(keyName, true) ?? subkey.CreateSubKey(keyName, true);

            return new(targetKey);
        }

        internal static R.RegistryKey GetRootKey(RegistryRoot registryRoot)
        {
            if (registryRoot == RegistryRoot.LocalMachine) return R.Registry.LocalMachine;
            if (registryRoot == RegistryRoot.ClassesRoot) return R.Registry.ClassesRoot;
            if (registryRoot == RegistryRoot.CurrentUser) return R.Registry.CurrentUser;
            if (registryRoot == RegistryRoot.Users) return R.Registry.Users;
            
            return R.Registry.CurrentConfig;
        }

        private readonly R.RegistryKey Root;

        private Registry(R.RegistryKey subkey)
        {
            Root = subkey;
        }

        public T? Get<T>(string name)
        {
            var ret = (T?)Root.GetValue(name);
            return ret ?? default;
        }

        public void Set<T>(string name, T value) where T : notnull
        {
            Root.SetValue(name, value);
        }

        public void Dispose()
        {
            Root.Close();
            Root.Dispose();
        }
    }
}
#pragma warning restore CA1416 // Validate platform compatibility -> App is only for Windows