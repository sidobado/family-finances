﻿using Microsoft.Win32;

namespace ZefirLib.Reg
{
    abstract public class Base
    {
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        virtual protected RegistryKey Key { get; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    }
}
