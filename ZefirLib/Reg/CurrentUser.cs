﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using R = Microsoft.Win32;

#pragma warning disable CA1416 // Validate platform compatibility -> App is only for Windows
namespace ZefirLib.Reg
{
    public class CurrentUser : Base
    {

        protected override RegistryKey Key => R.Registry.CurrentUser;

        private string Software { get; set; }

        public CurrentUser(string softwareName)
        {
            Software = softwareName;
        }

        public T Get<T>(string name) where T : notnull, new()
        {
            var k = Key.OpenSubKey("Software\\" + Software);

            if (k != null)
            {
                var ret = (T)(k.GetValue(name) ?? new T());
                k.Close();
                return ret;
            }
            
            return new T();
        }

        public void Set<T>(string name, T value) where T : notnull, new()
        {
            var key = Key.OpenSubKey("Software\\" + Software);
            key ??= Key.CreateSubKey("Software\\" + Software);
            key.SetValue(name, value);
        }
    }
}
#pragma warning restore CA1416 // Validate platform compatibility